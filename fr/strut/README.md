# Framaslides

[Framaslides](https://framaslides.org) est un service en ligne libre qui permet de créer, éditer, voir et partager ses présentations.

* Vous pouvez créer et éditer vos présentations, en choisissant le contenu et en déplaçant les slides par rapport aux autres pour créer des transitions 3D&nbsp;;
* Lancer le diaporama et le publier via une URL&nbsp;;
* Partager des présentations avec un groupe et vous inspirer des modèles publics.

Découvrez ses fonctionnalités et possibilités avec notre [exemple d’utilisation](exemple-d-utilisation.md).

Tutoriel pour bien débuter&nbsp;: [Comment créer sa première présentation](create_first_presentation.html)

Documentation générale&nbsp;:

* Édition des diapositives
  * [Images](pictures.html)
  * [Vidéos](videos.html)
  * [Arrière-plan](background.html)
  * [Raccourcis](shortcuts.html)

* Gestion des présentations
  * [Groupes](groups.md)

Le service repose notamment sur le logiciel libre [Strut](https://strut.io), les sources de Framaslides sont disponibles sur [Framagit](https://framagit.org/framasoft/framaslides/).

### Pour aller plus loin&nbsp;:
-   [Déframasoftiser Internet](deframasoftiser.html)
-   Essayer [Framaslides](https://framaslides.org)
-   Découvrir [Strut, la brique logicielle originelle](https://strut.io/)
-   Participer au [développement de Framaslides](https://framagit.org/framasoft/framaslides/)
-   Un service proposé dans le projet [Dégooglisons Internet](https://degooglisons-internet.org)
-   Son développement a été financé [grâce à vos dons](https://soutenir.framasoft.org).

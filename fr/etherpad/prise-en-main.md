# Prise en main

## Framapad (sans compte)

### Exporter et importer un pad

Vous avez la possibilité de transférer un pad avec son historique et ses couleurs d'un pad à un autre (voire d'un compte MyPads à un autre) en utilisant le format `etherpad`. Pour ce faire&nbsp;:

#### Export

  * cliquez sur l'icône <i class="fa fa-exchange" aria-hidden="true"></i>
  * sélectionnez&nbsp;: <i class="fa fa-file-powerpoint-o" aria-hidden="true"></i> Etherpad
  * enregistrez le fichier sur votre ordinateur

#### Import

  * créez un nouveau pad (dans votre autre compte MyPads par exemple)
  * cliquez à nouveau sur  <i class="fa fa-exchange" aria-hidden="true"></i>
  * cliquez sur le bouton **Parcourir…**
  * sélectionnez le fichier précédemment enregistré
  * cliquez sur le bouton **IMPORTER MAINTENANT**
  * à la question **Importer un fichier écrasera le contenu actuel du pad. Êtes-vous sûr de vouloir le faire ?** cliquez **OK**
  * votre pad est alors remplacé par l'import

### Donner un accès public à un pad

Par défaut l'accès aux pads MyPads nécessitent un compte, mais vous pouvez permettre l'accès à n'importe qui ayant l'adresse de celui-ci. Pour ce faire vous devez&nbsp;:

  1. cliquer sur l'icône <i class="fa fa-wrench" aria-hidden="true"></i> en face de son nom dans le dossier
  * décocher **Paramètres du dossier ?**
  * choisir **Public** dans la liste déroulante
  * cliquer sur le bouton **sauvegarder**

Pour repasser le pad en accès restreint ou privé, il suffit de recocher **Paramètres du dossier ?** et de sauvegarder.

### Historique

Pour voir les versions précédentes d'un pad vous devez&nbsp;:

  1. cliquer sur l'icône <i class="fa fa-clock-o" aria-hidden="true"></i>
  * déplacer le curseur ![image curseur de l'historique pad](images/pad_curseur-historique.png) pour voir les versions ; vous pouvez utiliser les flèches&nbsp;:
    * <i class="fa fa-step-forward" aria-hidden="true"></i> pour avancer d'une version
    * <i class="fa fa-step-backward" aria-hidden="true"></i> pour reculer d'une version

Vous pouvez ensuite copier/coller ou [exporter/importer](#exporter-et-importer-un-pad) le contenu de l'historique dans un pad si besoin est.

## Mypads (avec compte)

### Ajouter des utilisateurs

Vous pouvez ajouter des utilisateurs et des administrateurs à un dossier MyPads si vous êtes vous-même administrateur.

<p class="alert-warning alert">Vous ne pouvez pas ajouter un même compte en tant qu'administrateur <b>et</b> en tant qu’utilisateur simple.</p>

Pour ajouter des **administrateurs** vous devez&nbsp;:

  1. cliquer sur le dossier que vous souhaitez partager
  * cliquer sur le bouton **+ Partager l'administration**
  * ajouter l'identifiant ou l'adresse mail d'un utilisateur **ayant déjà un compte**
  * une fois les utilisateurs présents dans **Utilisateurs sélectionnés** cliquer sur **Sauvegarder**

Ces comptes pourront alors créer des pads, ajouter des comptes administrateurs, des utilisateurs etc…

Pour ajouter des **utilisateurs** vous devez&nbsp;:

  1. cliquer sur le dossier que vous souhaitez partager
  * cliquer sur le bouton **+ Inviter et gérer les utilisateurs**
  * ajouter l'identifiant ou l'adresse mail d'un utilisateur **ayant déjà un compte**
  * une fois les utilisateurs présents dans **Utilisateurs sélectionnés** cliquer sur **Sauvegarder**

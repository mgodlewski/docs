# Prise en main détaillée

Le but de cette doc est de vous présenter <https://framatalk.org>, un système de Vidéoconférence sécurisé et ne nécessitant aucune création de compte !



## Créer une vidéoconférence

1. Se rendre sur l'url <https://framatalk.org> avec le navigateur de votre choix (Firefox est mon préféré,  Chrome sera très bien aussi)
2. Dans la zone **Démarrer une nouvelle réunion** (Zone entourée en rouge sur l'image suivante), entrer le nom de la réunion (n'importe quel mot ou chaîne de caractères contenant des lettres (Majuscule ou Minuscules) et/ou des chiffres)

   ![Démarrer une nouvelle réunion](images/screen001.png)

3. Cliquer sur **Créer**
4. Autoriser l'utilisation de votre micro et de votre webcam
    ![Chrome](images/screen002-chrome.png)

    ![Firefox](images/screen003-firefox.png)

5. Et voilà, votre Vidéoconférence est créée ! Il ne vous reste plus qu'à partager le lien avec les personnes avec qui vous souhaitez converser, en cliquant sur **Copier** (Zone entourée en rouge sur l'image suivante)

    ![Copier le lien](images/screen004.png)


## Rejoindre une réunion

1. Cliquer sur le lien qui vous a été envoyé (par exemple <https://framatalk.org/test>)
2. Autoriser l'utilisation de votre micro et de votre webcam
      ![Chrome](images/screen002-chrome.png)

      ![Firefox](images/screen003-firefox.png)
3. Et voilà, vous êtes maintenant connecté(e) !


## Sur Mobile
1. Télécharger l'application Jisti meet
  * [Android](http://jitsi.org/android)
  * [iOS](http://jitsi.org/ios)
2. Cliquer sur le lien qui vous a été envoyé (par exemple <https://framatalk.org/test>), l'application devrait s'ouvrir par défaut.
3. (Alternative) : Lancer l'application et saisir le nom du salon ou l'adresse complète de votre salon (par exemple <https://framatalk.org/test>)

![Mobile](images/framatalk_saisie_salon.png)

4. Vous pouvez aussi mettre framatalk.org comme serveur par défaut en allant dans vos paramètres puis Paramètres (dans le screen suivant, remplacer <https://framatalk.org> par <https://framatalk.org>)

  ![Paramètres Mobile](images/framatalk_appli_url_serveur.png)

## Sources et Licences

Cette doc est en [Creative commons](https://creativecommons.org/licenses/by/4.0/deed.fr), mais si vous la ré-utilisez, merci de modifier l'URL du serveur pour correspondre au votre !
Je vous invite donc à consulter la [liste des instances Jitsi meet publique](https://framatalk.org/accueil/fr/info/).

---

### Sources

- Doc utilisateur : https://docs.framasoft.org/fr/jitsimeet/prise-en-main.html
- Installer son instance : https://framacloud.org/fr/cultiver-son-jardin/jitsi-meet.html
(Et https://framacloud.org/fr/auto-hebergement/ pour l'installation de nginx et du certificat Letsencrypt)

---

### Auteur

[Kourai](mailto:kourai+jitsi@yggz.org)

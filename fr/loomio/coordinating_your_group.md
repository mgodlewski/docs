# Coordonner votre groupe

En tant que coordinateur d‎’un groupe Loomio, vous êtes responsable de l‎’accompagnement du groupe dans les étapes d‎’apprentissage d‎’utilisation de nouveau logiciel, ainsi que dans l‎’apprentissage de nouveaux procédés de collaboration plus efficaces et de prises de décision communes. Les taches mentionnées ci-dessus ne sont disponibles que pour les coordinateurs de votre groupe.

## Ajout d‎’une nouvelle image de couverture

Lors de la première connexion à votre nouveau groupe Loomio, définis par défaut une image de couverture aléatoire, ainsi qu‎’un espace réservé pour le logo de votre groupe. Vous pourrez personnaliser l‎’apparence de votre groupe, en ajoutant vos propres images de couverture et logos. Vous pouvez téléverser une nouvelle image de couverture ou un nouveau logo en sélectionnant le bouton de téléversement (icône de caméra) dans l‎’une de ces deux zones.

## Gestion des membres

<img class="screenshot" alt="Page de gestion des adhésions" src="images/members_page.png" />

Les coordinateurs de groupe peuvent ajouter/supprimer des membres, allouer ou supprimer les permissions **coordinateurs**. Ces actions peuvent être réalisées depuis la page Membres, accessible depuis le lien **Gestion Membres** situé dans le menu **Options** de la page de groupe.

### Ajouter/supprimer des coordinateurs

Pour ajouter, vous devez&nbsp;:
  1. Sur la page d'accueil du groupe, dans l'encadré **Membres du groupe**
  * cliquer sur <i class="fa fa-ellipsis-h" aria-hidden="true"></i> en face du membre à passer en coordinateur
  * cliquer sur **Nommer coordinateur**

Pour supprimer, vous devez&nbsp;:
  1. Sur la page d'accueil du groupe, dans l'encadré **Membres du groupe**
  * cliquer sur <i class="fa fa-ellipsis-h" aria-hidden="true"></i> en face du membre à passer en coordinateur
  * cliquer sur **Retirer des coordinateurs**



### Ajouter/supprimer des membres de groupe

sur la page **Membre**, à côté de chaque membre, un [bouton **Suppression**&nbsp;: [Bouton de suppression de membre](images/remove_button.png). Ce bouton supprime le membre de votre groupe.

### Accepter/ignorer des demandes d‎’adhésion

Si vos [paramètres de visibilité du groupe](group_settings.html#group-privacy) autorisent les visiteurs à joindre votre groupe **sur demande**, un coordinateur devra approuver chaque demande. Dans ce cas, les coordinateurs de groupe recevront un courriel à chaque fois qu‎’un utilisateur effectuera une demande d‎’adhésion. Le lien, envoyé dans le courriel vous redirigera vers la page **Demandes d‎’adhésion**, où vous pourrez approuver ou décliner la demande.

Si une demande est en attente, elle sera aussi visible sur la page de votre groupe, dans la zone **Demandes d‎’adhésion**.

## Désactivation/archivage d‎’un groupe

<div class="alert-warning alert">
Il n'est pas possible de <b>supprimer</b> un groupe, mais vous pouvez le <b>désactiver</b>.
</div>

Désactiver votre groupe signifie que ni vous, ni aucun autre membre ne pourra accéder au groupe. Si vous souhaitez désactiver votre groupe, vous pouvez aussi le faire, en sélectionnant **Désactiver le groupe**, dans le menu **Options <i class="fa fa-caret-down" aria-hidden="true"></i>** de la page du groupe. Vous devez, bien sûr, avoir les droits nécessaires (admin).

Pour réactiver un groupe désactivé sur Framavox, il faudra en faire la demande sur https://contact.framasoft.org/fr/

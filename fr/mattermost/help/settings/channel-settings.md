# Paramètres du canal


_____

Les préférences de notification, l'en-tête du canal et le sujet du canal sont personnalisables pour chaque canal.

## Préférences de notification des canaux

Les préférences de notification peuvent être modifiées pour chaque canal dont vous êtes membre.

#### Envoyer les notifications

Par défaut, les préférences des notifications de bureau définies dans *Paramètres du compte* sont utilisées pour tous les canaux. Pour personnaliser les notifications de bureau pour chaque canal, cliquez sur le nom du canal en haut du panneau central pour accéder au menu déroulant, puis cliquez sur **Préférences de notifications > Envoyer des notifications de bureau**.


#### Marquer les canaux non lus

Personnalisez les événements qui affichent le nom des canaux en gras dans le menu déroulant puis dans **Préférences de notification > Marquer les canaux non lus**.

## En-tête du canal

L'en-tête du canal est le texte qui apparaît à côté du nom du canal en haut de l'écran. Il peut être utilisé pour résumer le sujet du canal, ou pour fournir un lien vers les documents fréquemment consultés.  N'importe quel membre du canal peut modifier l'en-tête en cliquant sur le nom du canal en haut du panneau central pour accéder au menu déroulant, puis en cliquant sur **Éditer la description du canal boite de dialogue**.

## Description du canal

Ce texte apparaît dans la liste des canaux dans le menu *Plus* et aide les utilisateurs à décider de rejoindre le canal ou non. N'importe quel membre du canal peut modifier la description du canal en cliquant sur le nom du canal en haut du panneau central pour accéder au menu déroulant, puis en cliquant sur **Éditer la description du canal boite de dialogue**.

## Archiver un canal

Vous pouvez archiver un canal&nbsp;: cela ne le supprimera pas complètement mais le masquera et le rendra inaccessible aux membres de l'équipe. Il ne sera possible de le restaurer qu'en [nous le demandant](https://contact.framasoft.org/).

Pour archiver un canal vous devez cliquer sur l'icône <i class="fa fa-caret-down" aria-hidden="true"></i> à côté du nom du canal puis cliquer sur **Archiver le canal**.

![image de comment archiver un canal](../../images/team-archive_canal.png)

Une *popup* s'ouvrira avec la question&nbsp;:

> Ceci va archiver le canal de l'équipe et rendre son contenu inaccessible de tous les utilisateurs.Voulez-vous vraiment archiver le canal Archivez-moi ?

Cliquez **Archiver**.

## Quitter un canal

<div class="alert-warning alert">
Si vous quittez un canal privé vous devrez vous faire inviter à nouveau !
</div>

Pour quitter un canal vous devez&nbsp;:

  * vous rendre sur le canal à quitter
  * cliquer sur le nom du canal en haut de la page
  * cliquer sur **Quitter le canal**

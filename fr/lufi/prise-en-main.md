# Prise en main

![framadrop explication numérotée](images/drop_mail_redaction_numero.png)

  1. bouton pour télécharger le fichier
  2. bouton pour copier le lien de téléchargement du fichier
  3. bouton pour [envoyer un mail avec le(s) lien(s)](#envoyer-les-liens-par-mail)
  4. bouton pour supprimer le fichier

## Envoyer les liens par mail

Une fois votre fichier déposé dans Framadrop, vous pouvez envoyer le lien par mail aux destinataires de votre choix. Pour ce faire, cliquez sur **Envoyer tous les liens par mail** ou sur l'icône <i class="fa fa-fw fa-envelope"></i> (voir `3` dans l'image ci-dessus)

Vous arrivez alors sur l'interface de personnalisation où vous pourrez :
  * indiquer les adresses destinataires dans le champ **Adresses mails séparées par des virgules** ; vous pouvez ne mettre qu'une seule adresse (sans virgule à la fin) ou plusieurs (séparées par des virgules : `adresse@mail.fr,adresse2@mail.fr,adresse3@mail.fr`)
  * indiquer un sujet dans **Sujet du mail**
  * personnaliser le corps du mail en enlevant ce dont vous n'avez pas besoin

Pour envoyer :

  * **Envoyer avec le serveur** : envoi directement depuis votre navigateur
  * **Envoyer avec votre propre logiciel de mail** : utilisera Thunderbird ou Outlook (ou autres)

![envoi de mail via le navigateur](images/drop_mail_redaction.png)

## Supprimer un fichier avant sa date d'expiration

Pour cela, vous devez, **depuis le même navigateur que celui utilisé pour l'envoi**&nbsp;:

  * cliquer sur l'onglet **[Mes fichiers](https://framadrop.org/files)**
  * cliquer sur l'icône <i class="fa fa-trash" aria-hidden="true"></i> au bout de la ligne du fichier que vous souhaitez supprimer

<div class="alert-info"><b>Rappel : Seuls les fichiers envoyés avec le navigateur web sont listés ici. Les informations sont stockées en localStorage : si vous supprimez vos données localStorage, vous perdrez ces informations.</div>

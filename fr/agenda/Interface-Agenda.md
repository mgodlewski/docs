# Utilisation de l'application agenda
[<i class="fa fa-arrow-left" aria-hidden="true"></i> Retour à l'accueil](../README.md)

Une fois connecté à Framagenda, vous arrivez sur une interface semblable à celle-ci.

![agenda-1](images/agenda-1.png)

La partie gauche de l'application vous montre les contrôles et réglages d'affichage, tandis que les événements s'affichent au centre.

# Vues

Framagenda propose trois modes de vue : la vue « Calendrier » par défaut et les vues « Semaine » et « Jour », qui sont plus proches d'un agenda. Vous pouvez changer cette vue en cliquant sur les boutons correspondants en haut à gauche. De plus, vous pouvez naviguer entre les mois, les semaines et les jours avec les boutons « Précédent » et « Suivant ». Enfin, vous pouvez directement accéder à un jour en cliquant entre ces deux boutons pour afficher une vue réduite du mois, ce qui permet de rapidement accéder à un jour.

![agenda-2](images/agenda-2.png)

# Événements

## Création

Comme un agenda est déjà créé par défaut à la création de votre compte, vous pouvez directement créer un nouvel événement dedans en cliquant dans l'interface, à la date où vous voulez créer l'événement. Si aucun agenda n'existe, il faut en créer un préalablement.

![agenda-3](images/agenda-3.png)

Une fenêtre s'affiche alors pour vous permettre d'entrer des informations de base sur l'événement. Si vous avez besoin de rentrer plus de détails, vous pouvez cliquer sur « Plus… » pour accéder à une interface plus complète.

Vous pouvez valider la création de l'événement en cliquant sur « Créer ». L'événement apparaît dans l'agenda avec la même couleur que l'agenda auquel il est rattaché.

## Édition

En cliquant sur l'événement, vous pouvez éditer ses propriétés. De plus, vous pouvez *glisser-déposer* un événement pour changer sa date ou son horaire facilement.

## Suppression

Vous devez cliquer sur un événement pour pouvoir le supprimer. Supprimer un agenda supprime également tous les événements lui étant rattachés.

## Participants

Vous pouvez ajouter des participants à un événement en entrant leur nom s'ils sont parmi vos contacts, ou bien directement leur adresse de courriel. Une fois les participants validés, ils recevront un courriel avec l'événement en pièce jointe sous la forme d'un fichier .ics.

# Agendas

![agenda-4](images/agenda-4.png)

## Création
Vous pouvez créer un agenda en cliquant sur le bouton susnommé. L'éditeur vous demande un nom et une couleur pour votre agenda.

## Actions

Il est possible de masquer un agenda avec tous ses événements en cliquant sur le nom de l'agenda. La pastille de couleur à gauche du nom de l'agenda disparaît alors et les événements de l'agenda sont masqués.

Le menu situé à côté du nom de l'agenda vous donne accès
* au module d'édition pour modifier le nom ou la couleur d'un agenda,
* au lien *CalDAV* de l'agenda pour la synchronisation avec un client,
* au téléchargement de l'agenda sous forme de fichier iCalendar (*.ics*).
* à la suppression de l'agenda

## Partage
L'icône de partage à côté du nom de l'agenda ouvre le module de partage. Il existe deux types de partages possibles :
* Le partage avec d'autres utilisateurs de Framagenda
* La publication, qui rend votre agenda public

### Partage entre utilisateurs
Il est possible de partager un agenda avec d'autres utilisateurs de Framagenda. Pour cela :

  1. cliquez sur l'icône partage
  * tapez l'adresse mail de votre contact
  * sélectionnez le nom du contact dans la liste

![partage entre utilisateurs](images/agenda-partage.png)

<p class="alert-warning alert"><b>Attention</b> : il est préférable d'utiliser <b>l'adresse mail</b> du compte avec lequel vous voulez partager votre agenda pour éviter de partager avec un compte ayant le même nom&nbsp;!</p>

Lorsqu'un agenda est partagé avec un autre utilisateur, celui-ci le voit dans la liste de ses agendas et il a accès aux événements de celui-ci.

Un agenda est partagé par défaut en mode « lecture seule », c'est-à-dire que l'utilisateur avec qui l'agenda est partagé verra les événements mais ne pourra les éditer.
Il est possible également d'autoriser l'écriture pour les bénéficiaires du partage : ils peuvent alors modifier des événements dans l'agenda, en créer ou en supprimer. Pour cela, vous devez cocher la case **peut modifier** :

![peut modifier partage agenda](images/agenda-partage-modifier.png)

Pour supprimer un partage, il suffit de cliquer sur l'icône *poubelle* au bout de la ligne du nom de l'utilisateur.

Note : Contrairement à l'application Fichiers, l'application Agenda ne permet pas (encore) la fédération, ce qui veut dire qu'il n'est pas possible de faire du partage avec des utilisateurs de serveurs différents.

### Suppression d'un partage

Pour supprimer **un partage d'agenda** (pas l'agenda lui même) vous devez&nbsp;:

  1. cliquer sur **…**
  * cliquer sur **Supprimer**
  * attendre 3 secondes et cliquer sur l'icône "poubelle"

![image montrant la suppression d'un partage](images/agenda_partage_supp.png)

# Publication

<div class="alert-warning alert">
La fonctionnalité permettant d’embarquer son agenda sur un site Internet ne fonctionne malheureusement plus. Ce problème est connu de l’équipe qui développe le logiciel que nous utilisons pour Framagenda. Par conséquent, il est possible que vous rencontriez le message suivant :
<blockquote>
  <p>L’agenda n’existe pas</p>
  <p>Vous avez peut-être obtenu un mauvais lien ou le partage de l’agenda a été annulé ?</p>
</blockquote>

ou

<blockquote>
  <p>Bloqué par une stratégie de sécurité de contenu</p>
  <p>Une erreur est survenue pendant une connexion à framagenda.org.</p>
  <p>Firefox a empêché le chargement de cette page de cette manière car sa stratégie de sécurité de contenu ne le permet pas.</p>
</blockquote>

Il n’y a, pour le moment, pas de solution.
(Ticket en anglais en relation avec ce bug : <a href="https://github.com/nextcloud/calendar/issues/169">https://github.com/nextcloud/calendar/issues/169</a>)
</div>

On peut également partager un agenda via un lien public, ce qui donne accès à l'agenda pour tous les possesseurs du lien. En accédant au lien, on a accès à l'interface de l'agenda en lecture seule (c'est-à-dire qu'il ne peut pas être modifié : seulement consulté).

La publication s'effectue en cliquant sur la case à cocher **Partager par lien public**, ce qui donne accès au lien public (en cliquant sur l'icône <i class="fa fa-link" aria-hidden="true"></i>). L'agenda peut également être *dépublié* de la même manière&nbsp;:

![gif partage lien public](images/agenda_partage_public.gif)

Lorsqu'un agenda a été publié il est possible d'envoyer un courriel à quelqu'un pour lui signaler la publication de ce calendrier en cliquant sur <i class="fa fa-envelope" aria-hidden="true"></i>.

En cliquant sur l'icône <i class="fa fa-link" aria-hidden="true"></i>, vous aurez accès à&nbsp;:

  * (**1**) un lien pour télécharger l'agenda au format iCalendar (`.ics`) en cliquant sur le bouton **Télécharger**
  * (**2**) un lien *CalDAV* pour ajouter l'agenda à son client de gestion de calendriers favori (voir la partie sur la synchronisation avec des clients) en cliquant sur le bouton **S'abonner**
  * une adresse *WebDAV* pour pouvoir ajouter l'agenda comme une souscription sur une autre instance NextCloud ou ownCloud (celle disponible dans la barre d'adresse de votre navigateur)
  * (**3**) un code d'intégration (*iframe*) en HTML pour pouvoir intégrer l'agenda dans une page web comme par exemple Wordpress (visible en cliquant sur **Embarquer**)

![capture d'écran présentant l'interface de l'agenda public](images/agenda_public.png)

# Abonnement à des calendriers
Il est possible de s'abonner à des agendas publics sur internet, comme par exemple le calendrier scolaire sur [data.gouv.fr](https://www.data.gouv.fr/fr/datasets/le-calendrier-scolaire-format-ical/) ou vos horaires de train issus de Captain Train.

Si vous souhaitez vous abonner au calendrier scolaire — Zone A (iCal) de [data.gouv.fr](https://www.data.gouv.fr/fr/datasets/le-calendrier-scolaire-format-ical/), vous devez&nbsp;:

  * cliquer sur <i class="fa fa-clipboard" aria-hidden="true"></i> dans l'encadré de l'agenda pour copier le lien `ical` dans le presse-papier
  * (dans Framagenda) cliquer sur **+ Nouvel agenda**
  * cliquer sur **Nouvel abonnement par lien**
  * appuyer sur la touche **Entrée** ou cliquer sur l'icône <i class="fa fa-arrow-right" aria-hidden="true"></i>


<div class="alert-info alert">
La mise à jour des données est, par défaut, d'une semaine (une donnée modifiée sur l'agenda pourra mettre jusqu'à une semaine à s'afficher sur votre calendrier).
</div>

Une fois que l'agenda distant est ajouté, vous voyez tous les événements de cet agenda dans l'interface de l'application en lecture seule (modification impossible de votre côté).

<p class="alert-info alert">
<b>Note</b> : Les abonnements ne sont pas synchronisables avec un client CalDAV. Sur <a href="Synchronisation/Android.md">Android</a> il est toutefois possible d'utiliser ICSx⁵ (<a href="https://play.google.com/store/apps/details?id=at.bitfire.icsdroid">Google Play</a>, <a href="https://f-droid.org/packages/at.bitfire.icsdroid/">F-Droid</a>).
</p>

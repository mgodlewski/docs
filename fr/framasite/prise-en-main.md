# Prise en main

## Suppression d'un site

Pour supprimer votre site vous devez :

  * vous connecter sur [frama.site](https://frama.site/)
  * cliquer sur le bouton **Paramètres de ce site** dans le cadre du site que vous souhaitez modifier
  ![Suppresion de site](images/admin_suppression_site.png)
  * cliquer sur le bouton **Supprimer mon site** en bas de la page
  * entrer votre mot de passe **administrateur** (celui utilisé pour entrer sur [frama.site](https://frama.site/))
  * optionnel mais conseillé : vous pouvez aussi exporter votre site
  ![admin confirmation suppression](images/admin_confirmation_suppression.png)
  * cliquer sur **Confirmer la suppression du site**

## Suppression de votre compte

<div class="alert alert-danger">
    Les sites et wikis suivants associés à votre compte framasite seront <b>purement et simplement supprimés</b>, avec tous leurs contenus&nbsp;!
</div>

Pour supprimer votre compte vous devez :

  * vous connecter sur [frama.site](https://frama.site/)
  * cliquer sur le bouton **Mon compte**
  * cliquer sur l'onglet **Informations personnelles**
  * cliquer sur **Supprimer mon compte**
  ![admin suppression compte](images/admin_suppression_compte.png)
    * entrer votre mot de passe **administrateur** (celui utilisé pour entrer sur [frama.site](https://frama.site/))
    * cliquer sur **Supprimer mon compte**

## Gestion des utilisateurs

Vous pouvez ajouter des utilisateurs à votre site : soit en tant que **simple** utilisateur avec des droits limités, ou en tant qu'**admin**.

Pour ce faire, vous devez :
 * vous connecter sur [frama.site](https://frama.site/)
 * cliquer sur le bouton **Paramètres de ce site** dans le cadre du site que vous souhaitez modifier
  ![Paramètres du site](images/site_parametre.png)
 * cliquer sur le bouton **Ajouter un utilisateur** sous la liste des utilisateurs déjà créés
 * entrer :
  * son adresse mail
  * son identifiant pour le site
  * son mot de passe
  * son nom complet (facultatif : sera **affiché publiquement** à la place de votre identifiant sur les pages de votre site, comme par exemple vos posts de blog ou commentaires.)
  * laisser cochée la case **Admin** pour en faire un administrateur du site (qui pourra modifier le site et sa structure) ou décocher la case pour en faire un simple utilisateur

  <p class="alert alert-warning">
    Dû à un bug, il n'est pas possible d'ajouter un utilisateur non-admin (voir <a href="https://framagit.org/framasoft/Framasite-/framasite/issues/139" title="Lien vers le ticket Framagit">le ticket en rapport</a>).
  </p>

## Création d'un wiki

Pour créer votre wiki vous devez cliquer sur le bouton **Créer un nouveau site** sur https://frama.site/ puis cliquer sur **Créer un wiki** dans la colonne **Wiki**. A partir de là vous allez devoir remplir les informations concernant votre futur **wiki**.

  * **Adresse du site** (ne peut être changé) Ce nom sera utilisé comme sous-domaine de votre site, c'est-à-dire le nom devant `.frama.wiki`. Il doit faire entre 2 et 25 caractères alphanumériques et peut comprendre des tirets `-`, mais pas en début et en fin de texte. Les majuscules ne sont pas prises en compte.
  * **Nom du site** le titre de votre wiki (Le nom de votre site - Vous pourrez modifier ces informations plus tard)
  * **Description du site**
  * **Gestion des contrôles d'accès** :
    * Wiki ouvert (lecture, écriture, envoi de fichiers pour tout le monde)
    * Wiki public (lecture pour tout le monde, écriture et envoi de fichiers pour les utilisateurs enregistrés) **Option par défaut**
    * Wiki fermé (lecture, écriture, envoi de fichiers pour les utilisateurs enregistrés uniquement)
  * Informations concernant **l'administrateur de votre wiki**
    * **Adresse email**
    * **Identifiant**
    * **Mot de passe pour ce site** Par mesure de sécurité, nous vous recommandons de choisir un mot de passe différent pour vos sites et cet espace d'administration. Ainsi, si le mot de passe administrateur de votre site devait être révélé, il ne comprometterait pas la sécurité d'autres sites.
    * **Nom complet** : Facultatif. Sera **affiché publiquement** à la place de votre identifiant sur les pages de votre site, comme par exemple vos posts de blog ou commentaires.

# De quels outils ai-je besoin&nbsp;?

Comme dans bien des domaines, les techniques supposent des apprentissages. Ainsi, lorsque vous avez acheté votre ordinateur, et comme il y a de fortes chances qu'il ait été équipé d'emblée d'un système d'exploitation, vous avez dû apprendre à vous en servir pour répondre à vos besoins du moment&nbsp;: relier l'ordinateur à votre box Internet, ouvrir le navigateur, faire les premières mises à jour…

Le problème rencontré par beaucoup d'utilisateurs en matière de logiciel est de savoir identifier de quoi ils ont besoin. Dans beaucoup de cas, freinés par le prix de certains logiciels ou par simple méconnaissance de la diversité, les utilisateurs se rabattent naturellement vers des utilisations non adaptées&nbsp;: essayer d'ouvrir des photos dans un logiciel de traitement de texte, être dans l'impossibilité de lire des formats vidéos et être obligé de renoncer, devoir payer un logiciel dont on utilise la version d'essai pré-installée sur une nouvelle machine, etc. Tant de situations très courantes amènent les utilisateurs à se décourager ou ressentir de grandes frustrations devant l'apparente complexité de l'informatique domestique.

Dans ce chapitre nous allons voir que les logiciels libres ne sont pas seulement des alternatives aux «&nbsp;grands&nbsp;» logiciels connus que «&nbsp;tout le monde utilise&nbsp;»&nbsp;: ils représentent autant de solutions diverses et sereines pour se servir d'un ordinateur de manière efficace. Nous pousserons alors un peu plus loin les cas d'usages pour parler des systèmes d'exploitation, des formats et des protocoles de communications.

## Les logiciels libres&nbsp;: s'y retrouver

D'un point de vue néophyte, les logiciels libres sont souvent assimilés à des logiciels gratuits, disponibles sur Internet et téléchargeables. Même si le terme «&nbsp;gratuit&nbsp;» n'est pas adapté, ce n'est pas une si mauvaise opinion&nbsp;: les logiciels libres sont très nombreux et sont autant de solutions pour accomplir des tâches même similaires, sans avoir obligatoirement à débourser de l'argent.

Ainsi, le premier avantage de l'offre en logiciel libre, c'est la diversité. Cette dernière s'oppose à l'idée d'un quelconque monopole sur un secteur d'application donné. Pour donner un exemple simple&nbsp;: en 2008-2009, pour satisfaire aux exigences de la Commission Européenne sur la libre concurrence, Microsoft a mis en place un écran de choix de navigateurs Internet pour ne pas imposer l'utilisation d'Internet Explorer. Les utilisateurs peuvent alors choisir le navigateur Mozilla Firefox, qui est un logiciel libre, et l'utiliser par défaut.

Comment s'y retrouver dans cette offre&nbsp;? Ce n'est pas évident. D'une part vous devez trouver le logiciel qu'il vous faut et d'autre part cela suppose de devoir identifier votre besoin de manière à affiner la recherche et trouver le bon logiciel.

Pour vous y aider, il existe au moins trois ressources&nbsp;:

1. [Framalibre](https://framalibre.org) est le projet historique d'annuaire de logiciel libre par Framasoft étendu à toutes les ressources libres (car il n'y a pas que les logiciels qui peuvent être libres). Dans cet annuaire, entretenu et augmenté par la communauté, vous pouvez entrer des mots-clés et afficher des alternatives pertinentes et des suggestions&nbsp;;
2. [Clibre](http://www.clibre.eu/) est un autre annuaire de logiciels libres avec un panel choisi&nbsp;;
3. Une page Wikipédia «&nbsp;[liste de logiciels libres](https://fr.wikipedia.org/wiki/Liste_de_logiciels_libres)&nbsp;» vous permet aussi de trouver des logiciels, classés par catégories d'activités.

Un point commun important entre ces annuaires est qu'ils vous renvoient systématiquement vers les sites officiels des logiciels en question. C'est crucial&nbsp;: si vous devez télécharger un logiciel pour l'installer sur votre machine, faites-le toujours depuis le site officiel (ou depuis les dépôts officiels de votre distribution GNU/Linux si vous êtes dans ce cas). D'une part vous êtes sûr d'obtenir la dernière version, et d'autre part vous diminuez le risque d'installer une version modifiée (et non validée) qui pourrait causer des dommages sur votre machine ou, pire, la pirater.

Une autre précaution à prendre est de vous assurer de la licence du logiciel que vous comptez utiliser. Comme nous allons le voir, la gratuité n'est pas l'apanage du logiciel libre et tous les logiciels gratuits ne sont pas dotés des meilleures intentions. Fiez-vous donc de préférence aux annuaires mentionnés ci-dessus.


## Les logiciels libres&nbsp;: qu'est-ce&nbsp;?

Une fois que vous avez installé un ou plusieurs logiciels libres et que vous commencez à les éprouver, il est temps de prendre un petit peu de recul sur ce qu'implique le caractère *libre* du logiciel libre.

Le mouvement du logiciel libre est né au début des années 1980, en réaction à une transformation de l'économie du logiciel. Auparavant, les programmes étaient fournis avec les machines de manière gratuite et ouverte afin de faciliter et optimiser l'emploi de ces machines. La communauté des programmeurs pouvait alors s'échanger ces programmes, les modifier et les adapter aux besoins… jusqu'au moment où certaines firmes en décidèrent autrement pour s'approprier l'innovation des logiciels et maîtriser les usages. Sous l'impulsion de Richard M. Stallman, un programmeur qui a assisté à cette transformation, le mouvement du logiciel libre est né avec l'idée qu'un programme doit demeurer accessible, ouvert, modifiable et diffusable. Richard M. Stallman fonda alors le [projet GNU](https://www.gnu.org/gnu/thegnuproject.fr.html) et, avec des amis, la [Free Software Foundation](https://www.fsf.org/?set_language=fr), aujourd'hui de renommée mondiale, qui a pour but de promouvoir et défendre le logiciel libre. Le projet GNU permit de mettre au point la Licence Publique Générale (GNU GPL), d'un point de vue juridique, qui définit le logiciel libre et les conditions d'usage du logiciel placé sous cette licence.

<div class="alert alert-info">
<p><img src="images/encarts/cloche.png" style="margin-right:10px;"><strong>Les quatre libertés du logiciel libre</strong></p>

<ol>
<li>la liberté d'exécuter le programme, pour tous les usages,</li>
<li>la liberté d'étudier le fonctionnement du programme et de l'adapter à ses besoins,</li>
<li>la liberté de redistribuer des copies du programme,</li>
<li>la liberté d'améliorer le programme et de distribuer ces améliorations au public, pour en faire profiter toute la communauté.</li>
</ol>

(Voir la page Wikipédia consacrée au « <a href="https://fr.wikipedia.org/wiki/Logiciel_libre">Logiciel libre</a> »)
</div>


Ces libertés impliquent&nbsp;:

* que le logiciel est disponible de manière égalitaire et sans restriction,
* que le logiciel est auditable par la communauté (il ne peut contenir de code malveillant ou si c'est le cas, très rapidement décelé),
* que le fonctionnement du logiciel est ouvert et permet d'innover sur cette base,
* que l'on peut donner des copies du logiciel, ou les vendre (ne serait-ce que pour couvrir les frais de main-d'œuvre),
* que l'on peut redistribuer le logiciel, y compris sous une version modifiée,
* que les modifications peuvent être elles aussi distribuées à toute la communauté.

Du point de vue individuel, que l'on contribue ou non au logiciel libre, le fait d'en utiliser implique&nbsp;:

* une limitation des risques de sécurité,
* une qualité dans les attentes concrètes du logiciel&nbsp;: il fera ce pourquoi il a été conçu,
* un suivi dans les améliorations du logiciel, à l'écoute de ses utilisateurs (même sans être programmeur vous pouvez faire part de vos remarques et difficultés et contribuer ainsi au développement),
* la possibilité de donner à vos proches une copie du logiciel ou les encourager à l'installer, sans contrainte.

Vous comprenez maintenant pourquoi les logiciels libres sont la plupart du temps gratuits et, s'ils sont payants, c'est parce qu'on y ajoute du service ou de l'expertise, ce qui fait d'ailleurs vivre beaucoup d'entreprises. Rien ne vous empêche, par ailleurs, d'encourager les développeurs en faisant des dons (bien souvent il existe près de chez vous des organisations à but non lucratif qui œuvrent pour le logiciel libre).

## Mes logiciels au quotidien

Dans un cadre domestique, les activités sur un ordinateur ne sont pas si nombreuses. Nous pouvons dresser un tableau des besoins les plus courants en identifiant de manière non exhaustive des logiciels libres adaptés aux différents exercices. Pour les besoins de ce tableau, nous listons des logiciels compatibles Windows.


| Activité | Logiciels non-libres | Logiciels libres |
|:-------- |:-------------------- |:---------------- |
| Traitement de texte | Microsoft Word | [LibreOffice Writer](http://www.libreoffice.org/), [Abiword](https://www.abisource.com/) |
| Tableur | Microsoft Excel | [LibreOffice Calc](http://www.libreoffice.org/) |
| Diaporama | Microsoft Powerpoint | [LibreOffice Impress](http://www.libreoffice.org/), [Sozi](http://sozi.baierouge.fr/) |
| Gestion de courriel | Microsoft Outlook | [Mozilla Thunderbird](https://www.mozilla.org/fr/thunderbird/),  [Claws Mail](http://www.claws-mail.org/) | 
| Dessin, retouche photos | Adobe Photoshop | [Gimp](https://www.gimp.org/) |
| Graphisme, PAO | Adobe Indesign | [Inkscape](https://inkscape.org/fr/), [Scribus](https://www.scribus.net/) |
| Lecture de documents PDF | Adobe Acrobat | [Evince](https://wiki.gnome.org/Apps/Evince), [Sumatra PDF](https://www.sumatrapdfreader.org/free-pdf-reader-fr.html) |
| Navigation Internet | Microsoft Internet Explorer | [Mozilla Firefox](https://www.mozilla.org/fr/firefox/new/) |
| Messagerie instantanée, voix, vidéo-chat | (Microsoft) Skype | [Pidgin](https://pidgin.im/), [Jitsi](http://jitsi.org/) |
| Vidéo | Microsoft Windows Media Player | [VLC](https://www.videolan.org/vlc/), [SMPlayer](http://smplayer.sourceforge.net/en/info) |
| Audio | Microsoft Windows Media Player  | [Amarok](https://amarok.kde.org/), [VLC](https://www.videolan.org/vlc/), [Audacity](http://www.audacityteam.org/) |


Vous remarquez que seules deux firmes apparaissent dans les logiciels non-libres… Par exemple, pour «&nbsp;mieux&nbsp;» intégrer ses logiciels avec le système d'exploitation, Microsoft a choisi de développer de multiples secteurs d'application. Nous n'en avons ici qu'un petit aperçu. Du côté des logiciels libres, de multiples acteurs produisent des alternatives, ce qui fait que le choix ne se fait pas seulement entre logiciels libres et logiciels non-libres, mais aussi en fonction des approches différentes d'un même besoin. Multiplier ces approches vous permettra de vous servir de ces logiciels libres de manière efficace, en choisissant d'utiliser à chaque fois le plus approprié.

## Focus sur la suite LibreOffice

Une suite bureautique est un ensemble intégré de logiciels permettant d'accomplir un ensemble de tâches liées à la production de documents bureautiques. La suite [LibreOffice](https://fr.libreoffice.org/) est la suite bureautique célèbre qui regroupe les applications suivantes&nbsp;:

* LibreOffice Writer&nbsp;: un logiciel de traitement de texte,
* LibreOffice Calc&nbsp;: un tableur,
* LibreOffice Impress&nbsp;: un créateur de diaporamas,
* LibreOffice Draw&nbsp;: pour la création de graphismes, schémas, diagrammes…
* LibreOffice Maths&nbsp;: un éditeur de formules mathématiques.

<div class="alert alert-info">
<p><img src="images/encarts/cloche.png" style="margin-right:10px;"><strong>Bon à savoir</strong></p>

Une des originalités de LibreOffice est de proposer un dépôt officiel d'<a href="https://extensions.libreoffice.org/">extensions</a> et de <a href="https://extensions.libreoffice.org/templates/">modèles</a> en tous genres. Vous y trouverez certainement ce que vous cherchez, entre fonctionnalités supplémentaires, packs d'icônes, modèles prêts à l'emploi…
</div>

L'interface de LibreOffice est assez classique et ressemble à celle de Microsoft Office. Pour une première utilisation, prenez le temps de faire quelques essais puis reportez-vous à la documentation sur le [Wiki de la Document Foundation](https://wiki.documentfoundation.org/Documentation/fr) (éditeur de Libreoffice). Vous y trouverez des manuels, des tutoriels, des vidéos, autant de contenus que la communauté et la Document Foundation mettent à disposition des utilisateurs finaux.

![LibreOffice Writer sous GNU/Linux Mint](images/screenLibreofficeWriter.png)

Un fichier produit avec LibreOffice portera une extension par défaut selon le logiciel avec lequel vous l'avez créé&nbsp;: `.odt` pour Writer, `.ods` pour Calc, `.odp` pour Impress, `.odg` pour Draw. Néanmoins, LibreOffice peut lire et enregistrer des fichiers dans de multiples formats, notamment les formats de la suite Microsoft `docx`, `.xlsx`, `.doc`, `.xls`, ou même `.rtf`, `.pdf`, etc. Ainsi le passage ou la transmission de documents d'une suite à l'autre se fait généralement sans problème pour un usage courant[^4].

Enfin, notez que des dictionnaires sont téléchargeables depuis le site officiel pour vous permettre d'utiliser le correcteur orthographique. Le projet  [Grammalecte](http://www.dicollecte.org/grammalecte/) est plus abouti et intègre aussi de la correction grammaticale. Il permet aussi la correction typographique.

![Les options dans Gramalecte](images/grammalecte-options.png)


## Je peux aussi essayer des logiciels libres

Si l'installation de logiciels libres (ou l'installation d'une distribution GNU/Linux —&nbsp;cf. la fin de ce chapitre) vous intimide, il existe une solution facile : essayer des logiciels libres avec la [Framakey](https://framakey.org/Pack/Framakey-Mint).  Framakey est un projet de Framasoft consistant à rassembler sur une clé USB un ensemble de logiciels libres, intégrés en un système permettant de créer un bureau portable. En clair&nbsp;: vous disposez de vos logiciels sur une clé USB, vous pouvez la brancher sur n'importe quel ordinateur et utiliser ce bureau nomade, tout en enregistrant dessus (les données sont sauvegardées sur la clé). Très pratique, non&nbsp;?

Il existe plusieurs versions de la Framakey dont trois sont des distributions GNU/Linux *live*&nbsp;: Linux Mint, Salix et Ubuntu. Vous pouvez utiliser ce bureau nomade directement en branchant votre clé alors que votre actuel système d'exploitation est en route&nbsp;: vous obtenez alors une interface dans un bureau «&nbsp;virtuel&nbsp;» et vous pouvez utiliser les logiciels libres proposés. Ou bien vous *bootez* sur la clé, vous pouvez utiliser la distribution GNU/Linux et même l'installer de manière permanente sur votre machine.

Vous pouvez aussi [acheter cette Framakey](https://enventelibre.org/47-cles-usb), proposée par l'association  Framasoft à la vente, et ne pas avoir à effectuer les manipulations de création de la clé. Une autre solution est de vous rendre à un événement libriste (répertorié sur l'[Agenda du Libre](https://www.agendadulibre.org/))&nbsp;: sur les stands il est souvent possible de demander des clés similaires proposées comme *goodies* par les associations présentes (l'achat vous permet aussi de soutenir ces associations). 

![Interface de la Framakey sous Windows](images/FK112.png)




## Les formats et l'interopérabilité

Lorsque vous échangez des documents, des photos ou n'importe quel fichier, quel que soit le logiciel qui vous a servi à les produire, vous devez vous assurer d'une chose au moins&nbsp;: que votre correspondant soit en mesure de les utiliser. C'est un des principes élémentaires de partage et de circulation de l'information&nbsp;: l'interopérabilité des formats.

Un format de fichier est une convention qui représente la manière dont sont arrangées et stockées les données regroupées dans ce fichier (et une donnée est un ensemble de *bits*, c'est-à-dire des 0 et des 1). Ces conventions permettent d'échanger les fichiers. Pour les lire, les programmes sont donc censés adopter ces conventions.

L'interopérabilité est la propriété d'un format de pouvoir être lu par plusieurs programmes différents&nbsp;; c'est aussi un principe qui considère qu'un format de fichier ne doit pas être réservé à un programme en particulier. En effet, si l'éditeur d'un logiciel conçoit un programme capable de lire et d'enregistrer des fichiers uniquement lisibles par ce programme, les utilisateurs sont condamnés à l'utiliser exclusivement. Si l'éditeur ferme ses portes ou change les formats sans assurer la maintenance des formats qu'il utilise à plusieurs années d'intervalles, vos fichiers deviennent inutilisables. C'est un peu comme si vous achetiez une voiture à essence avec l'obligation de vous rendre dans des stations-services d'une marque en particulier pour acheter la seule essence compatible.


Un bon format interopérable est censé être aussi ouvert et, mieux, libre&nbsp;: c'est-à-dire que ses spécifications techniques sont documentées et connues de tout le monde de manière à ce que les programmeurs puissent concevoir des logiciels capables de produire et lire des fichiers dans un format devenu de fait un *standard*. Par exemple, le format Open Document (ODF, Open Document Format) connu pour être produit par les logiciels de la suite LibreOffice (mais pas uniquement), est devenu en 2006 une norme ISO. Ce qui est loin d'être le cas pour d'autres formats pourtant bien connus comme le `.doc` dont Microsoft a par ailleurs cessé le développement au profit de OpenXML.

<div class="alert alert-info">
<p><img src="images/encarts/cloche.png" style="margin-right:10px;"><strong>Ouvert ne signifie pas libre</strong></p>

Certains formats ouverts ne sont pas pour autant libres. Un format ouvert peut très bien devenir fermé. Un format libre est soumis à une licence libre (voir le chapitre 2), ce qui permet de verser les spécifications dans les biens communs (si son développement s'arrête, il pourra être repris par d'autres).
</div>

Enfin, le format Open Document étant une norme ISO, il est naturellement encouragé dans tous les pays de l'Union Européenne, cette dernière ayant [publié plusieurs recommandations](http://ec.europa.eu/idabc/en/document/3439/5887.html) pour son adoption dans les administrations publiques. En France, le [Référentiel Général d'Interopérabilité](http://www.modernisation.gouv.fr/ladministration-change-avec-le-numerique/par-son-systeme-dinformation/le-referentiel-general-interoperabilite-fait-peau-neuve) décrit les normes et les pratiques de l'administration publique. La version 2.0 du Référentiel a été [adoptée officiellement en 2016](https://www.legifrance.gouv.fr/eli/arrete/2016/4/20/PRMJ1526716A/jo/texte), et fait du format ODF la seule norme recommandée dans les administrations publiques. Hélas, beaucoup d'entre elles sont encore [prisonnières](http://www.zdnet.fr/actualites/l-etat-francais-a-paye-539-millions-d-euros-a-microsoft-pour-ses-logiciels-en-2011-39790384.htm) des formats de la suite de Microsoft…

### Bureautique

Dans le domaine de la bureautique, l'échange de fichiers donne lieu bien souvent à des contrariétés&nbsp;:  recevoir un fichier et ne pas pouvoir l'ouvrir (ou bien en affichant des suites de caractères abscons), découvrir que votre diaporama est illisible sur l'ordinateur dédié à la vidéo-projection, s'apercevoir que la mise en page du document n'est plus valable d'une version d'un logiciel à l'autre, etc. Bien qu'à notre connaissance aucune étude n'ait  vraiment été réalisée à ce sujet, toutes ces difficultés liées à l'utilisation des formats de fichiers fait perdre énormément de temps à tout le monde.

#### Enregistrer dans le bon format

Lorsque vous rédigez un document, avec un traitement de texte ou un tableur, vous avez tout intérêt à choisir un logiciel qui vous laissera le choix des formats d'enregistrement. C'est le cas de LibreOffice qui, au moment d'enregistrer un document, en utilisant la fonction `Fichier > Enregistrer sous…`, vous propose un grand choix de formats, normalisés ou non.

![Choisir le format de fichier au moment de l'enregistrement](images/libreofficeformat.png)

Il vous appartient d'enregistrer un document en fonction de ce que vous voulez en faire. Vous pouvez choisir le format Open Document si vous voulez que votre fichier soit pérenne ou si vos correspondants utilisent des logiciels capables de le lire. Si en revanche vous connaissez les logiciels de votre correspondant, choisissez un format qu'il sera capable d'utiliser (ou encouragez-le à utiliser la suite libre et gratuite LibreOffice).

#### Utiliser le format PDF

Un autre moyen d'assurer l'interopérabilité est d'utiliser un format indépendant du logiciel avec lequel vous créez ou lisez un fichier. Il en va ainsi du format PDF, créé par la firme Adobe mais devenu une norme ISO. Tous les lecteurs PDF ne respectent pas cette norme, mais dans le cas de documents «&nbsp;simples&nbsp;», l'envoi au format PDF vous assure une certaine similitude entre ce que vous désirez que votre correspondant puisse voir et ce qu'il verra effectivement. De la même manière, vous pouvez manipuler un fichier PDF sur plusieurs machines et systèmes différents, notamment pour vos diaporamas&nbsp;: avec LibreOffice Impress, vous pouvez sauvegarder votre présentation en PDF et la lire le moment venu avec un lecteur PDF en mode plein écran.

Avec LibreOffice, l'utilisation du format PDF est intégrée et particulièrement efficace. On peut noter&nbsp;:

* la possibilité de compresser les images, de manière à ce que votre document ne soit pas trop lourd pour un affichage écran (surtout si vous n'avez pas travaillé les images avant de les inclure dans votre document),
* la possibilité d'utiliser la norme ISO,
* la possibilité d'intégrer le document source au format ODF dans le PDF, de manière à pouvoir éditer le document par la suite.

Pour créer un PDF avec LibreOffice, rendez-vous dans le menu `Fichier > Exporter au format PDF`, puis sélectionnez les options désirées avant d'enregistrer.

![Choisir les options PDF pour l'export dans LibreOffice](images/libreofficeoptionsPDF.png)

#### Utiliser du texte

Enfin, un troisième moyen pour assurer la pérennité de vos documents, c'est de les produire dans un format texte et de laisser faire le logiciel pour ce qui concerne la mise en page et la production finale. En effet, de plus en plus de logiciels proposent cette possibilité, en particulier les logiciels de prise de notes ou des éditeurs web comme ceux que vous pouvez rencontrer dans des applications comme le réseau social Diaspora* (dont nous parlons dans le chapitre 4).

Qu'est-ce que cela veut dire&nbsp;? Vous utilisez l'un des formats les plus basiques qui soient pour écrire votre texte avec un *éditeur de texte*, comme le font les programmeurs. Moyennant l'apprentissage d'un langage très simple comme le Markdown, vous laissez le logiciel interpréter ce langage pour produire un document mis en page. Avantages&nbsp;: vous ne vous occupez plus de la mise en page, vous vous concentrez sur ce que vous écrivez, vous savez que vous pourrez toujours récupérer votre production car elle est au format texte, le format de sortie est généralement interopérable[^5].



L'exemple du Markdown est éloquent. C'est une solution de traitement de texte qui nécessite un temps d'apprentissage d'environ 5 minutes. Voici quelques exemples.

Pour écrire un paragraphe, vous écrivez «&nbsp;au kilomètre&nbsp;» et vous passez une ligne entre chaque paragraphe en appuyant sur la touche `Entrée`; pour écrire un mot en italique, vous l'entourez d'une astérisque de chaque côté, pour l'écrire en gras, c'est deux astérisques. Pour créer un lien on utilise des crochets et des parenthèses. Un titre de niveau 1 s'introduit avec un croisillon, un titre de niveau 2 s'introduit avec deux croisillons, un titre de niveau 3 s'introduit avec trois croisillons, etc. Voici un court texte en Markdown&nbsp;:


```
# La fête de Juliette

À l'occasion de la grande soirée donnée en son honneur au 
[château de Combrie](http://chateaucombrie.com), Juliette
remarqua la *veste damassée* que portait Nicolas, le 
jardinier qui fit tant d'efforts pour paraître élégant
ce soir-là.

C'est bien lui qui avait successivement ramassé et soigné&nbsp;:

* un petit écureuil roux,
* une mésange charbonnière,
* Gaston, le hérisson.

```


Pour écrire en Markdown, le mieux est d'utiliser un logiciel disposant d'un système de coloration syntaxique (vous visualiserez alors mieux les marques de formatage), et éventuellement un panneau latéral permettant de visualiser en direct un aperçu de mise en page. Il existe beaucoup d'éditeurs Markdown. En voici trois assez différents, et libres&nbsp;:

* [Ghostwriter](http://wereturtle.github.io/ghostwriter/), un éditeur de texte couplé à un panneau de visualisation directe&nbsp;; il permet aussi d'exporter aux formats ODT, PDF et HTML,
* [Retext](https://github.com/retext-project/retext), un éditeur similaire à Ghostwriter,
* [Stackedit.io](https://stackedit.io/)&nbsp;: une interface web qui transforme votre navigateur en éditeur Markdown,
* [Typora](https://typora.io)&nbsp;: un éditeur qui intègre le résultat des commandes Markdown à la volée.

![Éditeur Retext et panneau de visualisation de la mise en page en HTML](images/screen-retext.png)



### Manipulation d'images

Les différences de formats de fichier d'image ne sont pas faciles à comprendre. Si vous ne les connaissez pas, il vous sera difficile de les manipuler, ce qui est paradoxal à l'époque où tout le monde peut prendre des photos de grande qualité grâce à toutes sortes d'appareils numériques. Par exemple, si vous destinez une photographie à n'être vue que sur un écran d'ordinateur, il ne vous est pas nécessaire de l'envoyer en haute définition à votre correspondant. Si au contraire vous voulez faire développer une photographie, ou si vous devez imprimer une image sur un poster, il va falloir faire attention à sa définition, son contraste, son degré de compression, etc.

Beaucoup de surprises seraient évitées si ces formats de fichiers étaient utilisés à bon escient. Procédons à un comparatif. 

|Dénomination     | Abbréviation   | Définition                              | Disponibilité  |
|:--------------- |:-------------- |:--------------------------------------- |:-------------- |
| Joint Photographic Experts Group  | jpg, jpeg, JPG, JPEG | Méthode de compression d'une image fixe (matricielle) et restitution. Plusieurs niveaux de compression disponibles altérant ou non la qualité. | Format ouvert (créé par le groupe du même nom) |
|  Portable Network Graphics | PNG | Spécification (et norme) pour Internet, surtout pour des shémas, graphiques, icônes… Permet aussi l'enregistrement de photographies sans perte de données. | Format ouvert (créé par W3C) |
| Graphics Interchange Format | GIF | Format sans perte moins puissant que PNG. Destiné au web. | Format ouvert (créé par CompuServe) |
| Tag(ged) Image File Format | TIFF | Un conteneur d'images (à la manière de ZIP), qui peuvent être de différents formats. | Format ouvert (créé par Adobe) |
| Désignation Raw (brut, en anglais) | raw |  Désigne un type d'images au format «&nbsp;natif&nbsp;» issues des appareils numériques avant leur compression éventuelle en JPG. Permet de travailler directement sur l'image telle quelle, et même reprendre une photo en modifiant les paramètres de prise de vue. | N/A |
| Scalable Vector Graphics | SVG | Format de *données* pour graphismes vectoriels. Il s'agit de code XML qui *décrit* le graphisme. L'échelle ne change pas la qualité du rendu affiché. | Format ouvert (créé par W3C) |

Comme on le voit, ces formats de fichiers d'image sont adaptés à différents usages. Là aussi, plusieurs logiciels libres sont à votre disposition&nbsp;:

* [GIMP](https://www.gimp.org/) (GNU Image Manipulation Program)&nbsp;: un logiciel d'édition et de retouche d'image. Il permet de manipuler un très grand nombre de formats d'images. Le format de sauvegarde est XCF (afin de sauvegarder un travail en cours), mais il est possible d'exporter dans n'importe quel format. C'est l'outil idéal, par exemple, pour convertir d'un format à l'autre.
* [Rawtherapee](http://rawtherapee.com)&nbsp;: il permet de travailler avec des images RAW, c'est à dire directement sorties de votre appareil photo, avec toutes les options pour changer les paramètres.
* [Luminance HDR](http://qtpfsgui.sourceforge.net/)&nbsp;: il permet de traiter des images en vue de pratiquer de l'imagerie à large gamme (*High dynamic range*).
* [Inkscape](https://inkscape.org/en/)&nbsp;: un logiciel spécialisé dans l'édition de format SVG, idéal pour créer des graphismes adaptés à toutes sortes d'usages&nbsp;: web, impression, iconographie…

<div class="alert alert-info">
<p><img src="images/encarts/cloche.png" style="margin-right:10px;"><strong>Remarque</strong></p>

N'hésitez pas à bien identifier ce dont vous avez besoin. Par exemple, il est possible de traiter des images RAW et même reproduire des effets HDR avec Gimp. Cependant, ce n'est pas son activité de prédilection. De la même manière vous pouvez produire du SVG avec Gimp, mais vous pouvez aussi importer une image dans Inkscape et la vectoriser. 
</div>

![Traitement d'image RAW avec Darktable sous GNU/Linux](images/screen-darktable.png)

### Audio et vidéo

Tout comme les formats de fichiers d'images, les «&nbsp;formats&nbsp;» audio et vidéos correspondent à des usages différents. Même si cette appellation est générique, vous avez certainement entendu parler de *codecs* à l'occasion d'une lecture d'un fichier audio ou vidéo. Un codec est un dispositif (logiciel) servant à compresser ou décompresser un signal numérique, avec plus ou moins d'efficacité, avec plus ou moins d'options. Par exemple un codec vidéo peut avoir des outils algorithmiques capables de compenser les effets de traînée, ou corriger des mouvements au quart de pixel. 

Un logiciel de lecture vidéo ou audio (ou d'image) a besoin des codecs correspondant aux types de fichiers qu'on veut lui faire lire, y compris si les fichiers se trouvent en ligne et accessibles via le navigateur (ce dernier fait appel au programme utile en temps voulu). En l'absence de codecs adéquats, le fichier ne pourra pas être lu.

Certains codecs sont libres, d'autres sont seulement ouverts. Il faut aussi distinguer les codecs qui donnent lieu à des extensions de fichiers (comme `.mpg` ou `.mp3`) et les *conteneurs* qui sont des fichiers (reconnaissables à leurs extensions propres, comme `.avi` ou `.mkv`) mais qui en fait contiennent des fichiers multimédias qui sont encodés selon des codecs différents. Par exemple, un fichier `.mkv` peut contenir un film dont le son est encodé en OGG et la vidéo en MPEG. Voici quelques codecs et conteneurs&nbsp;:



|Dénomination | Nature  |  Définition                            | Disponibilité                |
|:----------- |:------- |:-------------------------------------- |:---------------------------- |
| Xvid        | Codec | Norme MPEG (compression) | Libre |
| DivX        | Codec | Norme MPEG (compression) | Ouvert et non-libre |
| MPEG 1, 2, 4, 7, 21 (Moving Picture Experts Group) | Norme | Normes audio et vidéo concernant le stockage, l'encodage, la diffusion… | Ouvert et non-libre |
| MP3 ( MPEG-1/2 Audio Layer III) | Format | Couche de la norme MPEG-1 pour la compression audio | Ouvert et non-libre |
| AVI (Audio Video Interleave | Conteneur | Différents codecs peuvent être encapsulés dans un fichier AVI | Ouvert et non-libre |
| FLAC (Free Lossless Audio Codec) | Codec | Permet la compression audio sans perte. | Libre |
| Vorbis (extension `.ogg`) | Codecs et formats | Issu de la fondation Xiph.org qui fournit les codecs et formats OGA (audio) et OGV (vidéo). | Libre |
| MKV (Matroska) | Conteneur | Différents codecs peuvent être encapsulés dans un fichier MKV | Libre |


Évidemment, il n'est pas facile de s'y retrouver… L'essentiel est de retenir qu'un logiciel qui permet de lire des fichiers multimédia doit aussi intégrer les bons codecs. C'est la raison pour laquelle, sur le mode d'emploi de votre chaîne Hi-Fi ou de votre smartphone, vous avez une liste des «&nbsp;formats&nbsp;» que peut lire votre dispositif (le logiciel installé dedans). Dans le cas des smartphones comme dans le cas des ordinateurs, heureusement, on peut choisir ses logiciels. 

Notez que ce n'est pas parce qu'un logiciel est libre qu'il ne lit que des formats libres. À vous de  voir si vous désirez installer des codecs non-libres… tout en utilisant, par exemple, les logiciels libres suivants&nbsp;:

* [VLC media player (VLC)](http://www.videolan.org/vlc/)&nbsp;: son grand intérêt est qu'il dispose d'emblée d'un très large panel de codecs audio et vidéo, ainsi que sa polyvalence. Son utilisation sur ordinateur, smartphone ou tablette peut aisément remplacer tous les autres logiciels de lecture. Il est de même capable de lire des flux vidéo (comme la télévision en ligne) ou des podcasts, voire devenir un serveur multimédia. Il peut aussi enregistrer… L'installer et l'utiliser par défaut sur votre machine, y compris comme greffon pour votre navigateur Internet, n'est pas un mauvais choix.
* [Mplayer](http://mplayerhq.hu) ou [Smplayer](http://smplayer.sourceforge.net/)&nbsp;: le second est inspiré du premier. Ils prennent en charge un grand nombre de codecs audio et vidéo, à l'image de VLC.
* [Amarok](https://amarok.kde.org/) est un lecteur audio disposant d'une interface facile à prendre en main. Il dispose de greffons très intéressants comme celui qui permet de récupérer sur Wikipédia diverses informations sur l'artiste que l'on écoute.


![Le logiciel VLC](images/screen-vlc.png)




## Les protocoles sur Internet

Pour terminer ce chapitre sur les logiciels et les formats, il est indispensable de parler des dispositifs techniques sans lesquels la communication informatique ne pourrait pas s’effectuer. Les protocoles dont nous allons parler sont les protocoles réseau, ils sont regroupés sous le nom de suite TCP/IP et ce sont sans doute ceux dont vous entendrez le plus parler si vous cherchez à établir une connexion Internet. Tout comme les formats, il est important de comprendre que ces protocoles de communication sont des standards&nbsp;: déjà présents dès l'origine d'Internet, c'est grâce à eux que le réseau s'est construit. 

<div class="alert alert-info">
<p><img src="images/encarts/cloche.png" style="margin-right:10px;"><strong>Bon à savoir</strong></p>
Un protocole de communication est une série d'étapes à suivre pour&nbsp;:

<ol>
<li>établir une connexion avec une autre machine,</li>
<li>définir la manière dont les informations seront échangées.</li>
</ol>
</div>

TCP/IP est une suite de protocoles internet qui porte le nom des deux principaux protocoles TCP et IP. Pour normaliser tous ces protocoles, un modèle de connexion entre ordinateurs a été développé par l'ISO sur la base de TCP/IP et nommé le *modèle OSI* '(Open Systems Interconnection).

Pour résumer rapidement TCP/IP, on peut dire que cette suite de protocoles est organisée en plusieurs niveaux (couches) qu'on peut représenter sous la forme du tableau suivant&nbsp;:


|     | Couche | Exemple | Rôle                                                                       |
|:---:|:------ |:------- |:-------------------------------------------------------------------------- |
| 5 | *Les applications* | HTTP, FTP | Des logiciels permettent d'assurer l'interface homme-machine, comme un navigateur Internet qui envoie et reçoit des requêtes HTTP |
| 4 | *Le transports des données* | TCP | Assurer le transport des données (flux). Par exemple&nbsp;: ouverture de la connexion, transfert, fin de la connexion. |
| 3 | *Le réseau* | IP | La structure du réseau. Pour IP, c'est l'adressage&nbsp;: chaque machine a une seule adresse |
| 2 | *Les liaisons* | Ethernet | La méthode de connexion pour transporter les données. Par exemple la commutation de paquets |
| 1 | *Couche physique* | ADSL, radio, satellite… | Applications physiques des techniques de communication |

On peut aussi comprendre cette suite de la manière suivante&nbsp;:

* l'accès au réseau (couches 1 et 2),
* Internet&nbsp;: une gigantesque ville dont IP (Internet Protocol) permet de gérer les adresses de chaque machine en assurant une fonction de *routage* (on parle alors des *adresses IP* qui identifient les machines sur le réseau),
* le transport des données&nbsp;: TCP (Transmission Control Protocol) transporte les données, les découpe en paquets,  contrôle leur réception, rend fiable les transferts d'informations,
* les applications permettent de traiter les données reçues et initient les envois éventuels.

Ainsi, lorsque vous naviguez sur Internet, toutes ces opérations doivent avoir lieu. Par exemple&nbsp;:

* votre système d'exploitation peut indiquer qu'il existe une connexion active, mais elle peut seulement concerner les premières couches c'est à dire, par exemple, la connexion entre votre machine et la box Internet de votre fournisseur d'accès. Cela ne signifie donc pas pour autant que vous êtes connecté au réseau Internet (seulement votre «&nbsp;réseau domestique&nbsp;»&nbsp;: vous avez bien une IP du type 192.168.1.XX mais votre box, elle, n'a pas d'IP qui lui permet d'être identifiée et de vous renvoyer des données)&nbsp;;
* vous pouvez avoir une connexion au réseau Internet mais si la couche applicative fonctionne mal (si votre navigateur est mal configuré) vous ne recevrez rien&nbsp;: pourtant vous êtes bel et bien connecté&nbsp;;
* d'autres problèmes peuvent survenir dans les couches intermédiaires, par exemple un routeur qui fonctionne mal et renvoie des paquets de manière erratique, etc.

D'autres protocoles de communication existent, nous en verrons davantage dans les chapitres suivants. L'essentiel, ici, est d'avoir une première approche du fonctionnement général d'Internet. C'est sur ce sujet que porte le reste de l'ouvrage. Dans la mesure où les autres couches sont des standards et sont censées assurer les niveaux et qualités de connexion des machines, la couche applicative (les logiciels que vous utilisez) vous concerne en tant qu'individu. C'est essentiellement à travers elle que vous pouvez agir sur le réseau, veiller sur la sécurité de vos données, choisir ce que vous recevez et ce que vous envoyez, prendre garde à ce que devient votre intimité numérique. Vos pratiques sont donc essentielles car elles déterminent votre mode d'existence sur un réseau dont nous venons de voir qu'il n'a rien de virtuel…




## Un système d'exploitation

Pour faire fonctionner les programmes (et les dispositifs matériels) les uns avec les autres, vous avez besoin d'un système d'exploitation (OS pour *Operating System* en anglais) qui regroupe les programmes nécessaires pour accomplir ces tâches. À l'achat de votre ordinateur, vous avez sans doute accepté que soit fourni l'OS. Bien sûr, cette vente liée vous permet d'avoir un ordinateur prêt à l'emploi une fois rentré chez vous. Sachez néanmoins que rien ne vous oblige à acheter une machine et un système d'exploitation imposé. 

Que vous souhaitiez installer Microsoft Windows, Mac OS (pour une mise à jour) ou GNU/Linux, le niveau de complexité est aujourd'hui similaire. On a longtemps accusé GNU/Linux d'être difficile d'accès, mais ces 10 dernières années ont vu fleurir des distributions de GNU/Linux spécialement adaptées au grand public. Dès lors, rien ne s'oppose vraiment à ce que vous choisissiez d'utiliser GNU/Linux, d'autant plus que si vous utilisez déjà des logiciels libres, le changement ne sera pas très difficile.

<div class="alert alert-info">
<p><img src="images/encarts/cloche.png" style="margin-right:10px;"><strong>Qu'est-ce qu'une distribution ?</strong></p>

GNU/Linux a ceci de particulier qu'on en parle en termes de <i>distributions</i>. Dans le secteur logiciel, une distribution est un ensemble cohérent de logiciels prêts à installer. Une distribution GNU/Linux est la plupart du temps composée des éléments suivants&nbsp;:

<ul>
<li>un noyau de système d'exploitation nommé Linux,</li>
<li>des programmes libres issus du projet GNU,</li>
<li>des programmes libres différents selon les distributions et qui leur donnent leurs saveurs particulières.</li>
</ul>
</div>

Il existe une multitude de distributions GNU/Linux différentes[^6], avec des noms plus ou moins exotiques&nbsp;: Debian  (sur laquelle sont basées beaucoup de distributions), Ubuntu, Fedora, Redhat, Suse, Linux Mint, Mageia… Elles ont aussi des bureaux (interfaces) différents, certains rappellant parfois ce que vous connaissez déjà, d'autres qui nécessitent un peu d'apprentissage… tout comme vous avez dû apprendre à vous servir de Windows 98, Windows 2000, Windows 7, Windows 10… Renseignez-vous sur ces distributions avant d'arrêter votre choix ou essayez-en plusieurs.



## Je veux essayer GNU/Linux

Dans cette section, nous allons tâcher de décrire rapidement les principales étapes pour installer un système GNU/Linux sur un ordinateur (ici pour la distribution [Linux Mint](http://www.linuxmint.com/)). Si vous ne souhaitez pas le faire, sautez cette section, vous pourrez toujours y revenir.

### Vocabulaire


* *image ou fichier iso*&nbsp;: il s'agit d'un fichier archive qui est une copie conforme d'un disque. Le fichier iso se manipule habituellement tel quel. Les distributions GNU/Linux sont presque toujours disponibles en tant que fichier iso. Cela permet de les graver sur un CD-rom ou de créer une clé USB afin  d'essayer et d'installer la distribution choisie.
* *Clé USB bootable*&nbsp;: le boot est la procédure d'amorçage d'un ordinateur. Un programme de boot est le programme initial que la machine va exécuter lorsqu'on la démarre. Une clé USB ou un CD-Rom bootable sont généralement des images disque fonctionnant de manière autonome. La machine démarre dessus et on peut ainsi tester le système d'exploitation présent sur le support. C'est encore le meilleur moyen de tester une distribution GNU/Linux (sans toucher au système existant de la machine présent sur son disque dur). D'un point de vue pratique, il est plus rapide de tester et d'installer à partir d'une clé USB car la vitesse de lecture sur ce support est généralement plus rapide que celle d'un lecteur de CD-rom (cela dépend aussi de la quantité de mémoire vive de la machine).
* *Un bureau*&nbsp;: il s'agit d'un agencement de fenêtres et de menus. Les habitués de Windows connaissent différents bureaux, depuis windows 98, en passant par Vista, windows Seven, 8 10, etc. Avec une distribution GNU/Linux, un bureau par défaut est installé mais on peut en changer simplement en installant celui de son choix. Certains bureaux sont graphiquement chargés, d'autres moins, plus ou moins gourmands en ressources, certains sont très ergonomiques, d'autres ressemblent à des terminaux dignes des années 1950, etc.


### Objectif

Nous souhaitons installer la distribution Linux Mint sur la machine après l'avoir testée grâce à une clé USB.

Pourquoi Linux Mint&nbsp;? C'est une question de préférence. Comparer différentes distributions se fait avant tout en fonction des besoins recherchés. Ici nous avons besoin d'une distribution généraliste et polyvalente. Linux Mint propose un bureau nommé Cinnamon, assez simple à appréhender pour commencer, et facilement configurable (il est toutefois possible d'en changer, y compris en prenant une version différente de Linux Mint (bureaux MATE, XFCE&hellip;)

### Matériel

Nous avons besoin&nbsp;:

* d'une clé USB vierge de 4Go, ce qui suffira amplement,
* d'un ordinateur doté ou non d'un système d'exploitation.


### Condition

Si vous possédez Windows il se peut que vous souhaitiez installer GNU/Linux «&nbsp;à côté de Windows&nbsp;». Nous ne traiterons pas de cette situation dans ce chapitre. Cela dit, pour expliquer rapidement&nbsp;: lors d'une installation normale, si la préexistence de Windows est détectée, le choix vous sera proposé par l'assistant, et le reste de l'installation se fera la plupart du temps de manière très aisée. Cependant, il est conseillé d'avoir une version de Windows la plus propre possible, défragmenter le disque au maximum (voire ré-installer Windows complètement), avant de procéder à l'installation de GNU/Linux.

Cette cohabitation des deux systèmes sur une même machine se nomme «&nbsp;dual boot&nbsp;». C'est au démarrage de la machine qu'on devra choisir quel système lancer. Reportez-vous à de nombreux tutoriels sur Internet, afin d'en savoir davantage.

### Les étapes

#### Télécharger la distribution

Téléchargez l'image ISO de Linux Mint en 32 ou 64 bits, selon la machine. Si vous ne savez pas si votre processeur est en 32 ou 64 bits, renseignez-vous ou prenez la version 32 bits.

Sur la [page de téléchargement](http://www.linuxmint.com/download.php) des versions de Linux Mint, vous avez le choix entre télécharger des versions 32 ou 64 bits équipées de bureaux différents&nbsp;: Cinnamon, MATE, KDE, Xfce. Cliquez sur votre choix. Une liste de miroirs sera alors proposée, choisissez le miroir le plus proche de chez vous et commencez le téléchargement.

#### Vérifier

Une fois l'iso téléchargée, il faut vérifier l'intégrité de l'image pour ne pas avoir de surprise lors de son utilisation. Cette opération est très rapide, optionnelle, mais d'une prudence élémentaire.  Sur la page des miroirs vous avez un lien `Verify your ISO`&nbsp;: il vous mènera vers les sommes de contrôle à vérifier. Il s'agit en gros d'une série de caractères qui attestent l'intégrité de l'image que vous venez de télécharger. Le but du jeu est de tirer la somme de contrôle du fichier ISO désormais présent sur votre machine et la comparer avec celle fournie sur le site (en fichier texte). Pour Linux Mint l'algorithme utilisé est SHA256.

Pour effectuer cette vérification, si vous êtes sous Windows, vous pouvez utiliser&nbsp;:

* [Quick Hash GUI](https://sourceforge.net/projects/quickhash/), un logiciel libre,
* [Hash Express](http://www.ecodified.com/ehashfast), un logiciel non-libre présent dans le Windows Store.

Téléchargez ensuite les sommes de contrôle présentées sur le site Linux Mint, puis effectuez la comparaison (les logiciels de vérification sont très simples d'utilisation).

#### Installer l'image sur une clé USB

Il vous faut un logiciel qui permet d'installer l'ISO sur la clé en la rendant bootable. Vous pouvez utiliser le logiciel [Unetbootin](http://unetbootin.sourceforge.net). En fait, ce logiciel permet même de télécharger la distribution de son choix, mais au moins vous aurez fait l'effort d'aller sur le site officiel de la distribution…

* choisissez la version de [Unetbootin](http://unetbootin.sourceforge.net) adaptée à votre actuel système d'exploitation et installez le logiciel,
* branchez la clé USB,
* lancez le logiciel Unetbootin,
* sélectionnez l'iso précédemment téléchargée,
* assurez-vous que le bon lecteur usb est sélectionné,
* lancez la procédure.


<div class="alert alert-info">
<p><img src="images/encarts/cloche.png" style="margin-right:10px;"><strong>Remarque</strong></p>

D'autres logiciels pour Windows permettent de créer de telles clés USB. On parle alors de distributions <i>live</i>&nbsp;:

<ul>
<li><a href="http://www.linuxliveusb.com/fr/home">Linux Live USB Creator</a> (LiLi),</li>
<li><a href="http://liveusb.info/dotclear/index.php?pages/Pr%C3%A9sentation">Multisystem</a>,</li>
<li>ou vous pouvez acheter directement une clé prête à l'emploi, par exemple sur le site <a href="https://enventelibre.org/47-cles-usb">En Vente Libre</a>.</li>
</ul>
</div>

#### Amorcer

Vous voici avec une clé chargée de potion magique&nbsp;: vous pouvez l'utiliser sans toucher au disque dur de l'ordinateur.

Branchez-la sur le PC sur lequel vous voulez installer GNU/Linux. Assurez-vous que ce PC est bien connecté à Internet (en filaire si possible parce que, même si la puce wifi a de grandes chances d'être correctement détectée lors de l'installation, le wifi est quand même moins rapide). Démarrez le PC.

Et là…

Il y a des chances qu'il démarre sur Windows (s'il en est équipé). Eh oui&nbsp;: chez certains fabricants, il y a une tendance à croire que toute la machine appartient à Windows, donc on ne cherche pas à permettre à l'utilisateur de choisir l'emplacement depuis lequel il veut que la machine démarre.

En fait, au démarrage de la machine, on voit souvent apparaître (rapidement) la possibilité d'appuyer sur une touche (type `F2` ou `F12`, ou `<esc>` / `Échap`, etc.) pour accéder au bios. Parfois même certains fabricants proposent une touche permettant d'accéder directement à un menu de `boot` permettant de choisir le support bootable.

Il faut donc être rapide et presser sur la bonne touche lorsque c'est proposé au démarrage. Si on accède au bios, tout dépend de sa configuration, mais généralement il y a un endroit où l'on peut choisir la séquence d'amorçage. En d'autres termes, il s'agit de dire à la machine&nbsp;: «&nbsp;lorsque tu démarres, tu vas d'abord chercher sur tel support s'il y a un système, et en l'absence de ce dernier, tu cherches sur un autre support&nbsp;». On peut ainsi agencer (souvent avec les flèches du clavier) la séquence d'ordre de démarrage entre le disque dur, l'USB, le lecteur CD-Rom, etc. Dans notre cas, nous mettrons le disque USB en tête de liste, puis `save and exit`. Pas de panique&nbsp;: à l'avenir, si on a une clé USB branchée lors du démarrage de la machine, dans la mesure où elle ne contient aucun programme d'amorçage, elle sera ignorée. Par contre celle que nous venons de créer avec Unetbootin, elle, contient un programme d'amorçage.

En redémarrant la machine, magie&nbsp;! c'est le menu de Unetbootin qui apparaît. Il propose alors de tester Linux Mint ou de l'installer, etc.

#### Essayer GNU/Linux

Le mieux est encore de tester. Outre le fait que vous pouvez ainsi essayer la distribution (elle sera alors dans la langue par défaut, c'est à dire souvent en anglais), vous pouvez aussi voir si elle fonctionne bien avec la machine.

Pour l'installation, soit vous la lancez depuis l'interface de Unetbootin, soit (si vous êtes en train de tester) en cliquant sur l'icône d'installation présente sur le bureau.

#### Installation

La première chose qui sera demandée est de sélectionner la langue.

Ensuite, il suffit de suivre les fenêtres qui s'afficheront. Tout est fait de manière très intuitive.

Attention&nbsp;: lorsque le choix se présentera, n'oubliez pas de retenir attentivement votre login et votre mot de passe, car ils vous serviront pour administrer la machine. 

Nous n'irons pas plus loin dans la procédure d'installation. Ici, le cas le plus simple est une machine sur laquelle vous allez *remplacer* votre actuel système d'exploitation par le nouveau. Évidemment, selon votre configuration, des choix plus spécifiques peuvent se présenter&nbsp;:

* si vous avez deux disques durs,
* si vous voulez installer GNU/Linux «&nbsp;à côté&nbsp;» d'un autre système comme Windows,
* si vous voulez partitionner votre disque de manière spécifique…

Dans tous les cas, ne vous lancez pas dans une configuration si vous ne savez pas ce que vous faites. Le mieux est de vous faire aider. Pour cela, vous avez certainement près de chez vous un ou plusieurs Groupes d'Utilisateurs de Linux (GUL). Vous pouvez en trouver la liste sur le site de l'[Agenda du Libre](http://www.agendadulibre.org/orgas).




[^3]: Nous les mentionnons ici selon la page Wikipédia consacrée au «&nbsp;[Logiciel Libre](https://fr.wikipedia.org/wiki/Logiciel_libre)&nbsp;».

[^4]: Si, par exemple, vos tableaux contiennent des macros spécifiques, la prudence est de mise.


[^5]: Sur le même principe, pour des utilisateurs plus avancés, il existe des distributions de LaTeX (voir [latex-project.org/](http://www.latex-project.org/)), à la fois langage et système de composition de document très puissant, utilisé notamment pour composer des documents scientifiques des essais…

[^6]: Voir la page Wikipédia «&nbsp;[Distribution Linux](https://fr.wikipedia.org/wiki/Distribution_Linux#Principales_distributions)&nbsp;» qui recense les principales distributions GNU/Linux.






# Fonctionnalités

## Afficher son nom
  1. En haut à droite, passer la souris sur la miniature de votre webcam,
  2. Double-cliquer sur le nom afficher, et entrer ce que vous voulez !
   ![Modifier son nom](images/screen005.png)

## Changer d'affichage

  1. En bas à droite, cliquer sur le bouton en forme de petits carrés,
   ![Passer en affichage Mosaic](images/screen006.png)
   ![Affichage Mosaic (Tous les participants)](images/screen007.png)

  2. Pour afficher un écran en particulier, cliquer simplement sur la vidéo que vous souhaitez voir en grand.

## Réafficher le lien et/ou ajouter un mot de passe à Vidéoconférence

  1. En bas à droite, cliquer sur le bouton **i**

    ![Bouton info](images/screen008.png)

  2. Les infos de la Vidéoconférence sont maintenant affichés,
  3. Cliquer sur "Copier" pour copier le lien et le partager à d'autres personnes.

  Pour rendre privé un salon vous pouvez lui donner un mot de passe. Toute personne arrivant sur le salon protégé par un mot de passe devra le saisir pour y accéder.

  4. Cliquer sur **Ajouter un mot de passe** si vous souhaitez mettre en place un accès plus limité à la Vidéoconférence (Il faudra alors le transmettre à vos co-conférenciers),
  5. Pour supprimer le mot de passe, re-cliquer sur **i** et **supprimer le mot de passe**.

  ![Bouton info](images/screen009.png)

## Mettre un mot de passe

Pour rendre privé un salon vous pouvez lui donner un mot de passe. Tout personne arrivant sur le salon protégé par un mot de passe devra le taper pour accéder.

Pour rendre un salon privé, vous devez&nbsp;:

  1. passer la souris dans le coin inférieur droit pour faire apparaître le <i class="fa fa-info" aria-hidden="true"></i>
  * cliquer sur <i class="fa fa-info" aria-hidden="true"></i>
  * cliquer sur **Ajouter un mot de passe**
  * taper le mot de passe
  * appuyer sur la touche **Entrée**

![gif de comment entrer un mot de passe](images/jitsi-mdp.gif)

## Partage d'écran

Pour partager votre écran ou une fenêtre en particulier, vous devez :

  1. Cliquer sur l'icône **écran** en bas à gauche de l'écran ![Bouton Écran](images/screen010.png)

  2. Sélectionner soit :

  * votre écran entier,
  * une fenêtre en particulier,
  * un onglet de votre navigateur.

    ![Bouton Écran](images/screen011.png)


  3. Cliquer sur **Share**.

Pour ne plus partager l'écran, vous devez cliquer à nouveau sur le bouton **écran**.

## Changer la langue

Pour changer la langue, vous devez&nbsp;:

  1. déplacer votre souris en bas de l'écran
  * cliquer sur <i class="fa fa-ellipsis-v" aria-hidden="true"></i> (**1**)
  * cliquer sur **Paramètres**

  ![Framatalk menu image](images/framatalk_menu.png)
  * cliquer sur **Plus**
  * changer **Langue**

  ![Framatalk paramètres plus image](images/Framatalk_parametres_plus.png)
  * cliquer sur **Ok**

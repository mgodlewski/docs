Sixième partie - Notifications et conversations
-----------------------------------------------

La dernière grande fonctionnalité que vous devez absolument connaître
sont les "conversations". Mais avant cela, voyons les notifications.

## Notifications

Un clic sur l'icône des notifications (qui ressemble à une cloche)
dans la barre d'en-tête affiche une liste déroulante des cinq
notifications les plus récentes. Si vous avez plusieurs notifications
non-lues, leur nombre apparaît en rouge par-dessus l'icône.

![Notify](images/notification-conversation-1.png)

Les notifications sont généralement libellées "La personne A a commenté
la publication de la personne B", "La personne A a commencé à partager
avec vous" ou "La personne A vous a mentionné dans une publication".
Depuis cette liste vous pouvez :

-   Cliquer sur le nom de la personne pour voir sa page de profil.
-   Cliquer sur message pour voir ce message.
-   Cliquer sur **Tout marquer comme lu**.
-   Cliquer sur **Tout afficher** pour vous rendre sur la page des
    notifications.

La page des notifications vous présente toutes les notifications qui
vous ont été adressées, 25 par page.

## Conversations

Une conversation est une communication privée entre deux personnes ou
plus. Pour accéder à cette fonctionnalité, cliquez sur l'icône
d'enveloppe dans la barre d'en-tête.

La page des conversations est divisée en deux colonnes : sur la gauche,
une liste des conversations dans lesquelles vous avez été engagé(e) ;
sur la droite, une vue de la conversation en cours (s'il y en a une).

![Conversations
page](images/notification-conversation-2.png)

Utilisez le bouton Nouveau message en haut de la colonne de gauche pour
démarrer une nouvelle conversation. Une fenêtre apparaîtra dans laquelle
vous pourrez écrire un message et ajouter des personnes à la
conversation.

Vous pouvez engager des conversations uniquement avec les gens avec qui
vous êtes en partage mutuel.

Lorsque vous tapez un nom sur la ligne "pour", des propositions
apparaissent, comme quand vous mentionnez quelqu'un. Cliquez sur le
nom de la personne que vous voulez contacter, ajoutez un sujet sur la
ligne "sujet", tapez votre message et c'est parti ! Aussi simple que ça.
Vous pouvez également utiliser le formatage de texte donc n'hésitez pas
à utiliser des titres et caractères gras dans votre message.

Dans la colonne de gauche, vous verrez une liste des conversations
comprenant le titre, la photo de profil de la personne qui l'a démarrée,
le nom de la dernière personne à y avoir répondu et, à droite, le nombre
de messages (en gris ; s'il y a des messages que vous n'avez pas lu,
cela sera indiqué en rouge), le temps depuis le dernier message et,
en-dessous, une icône "plusieurs personnes" si plus d'une personne (en
dehors de vous-même) est engagée dans la conversation. Passez votre
souris sur cette icône pour voir qui d'autre participe.

Vous pouvez lire une conversation en cliquant dessus dans la colonne de
gauche, ce qui la fera apparaître sur la droite. En haut à gauche, vous
verrez la liste des destinataires (y compris les personnes qui n'ont pas
encore contribué à la conversation). Assurez-vous d'y prêter attention
avant de vous lancer dans des commérages ! Il pourrait bien y avoir plus
de personnes engagées dans la conversation qui verront toutes votre
réponse.

Répondre dans une conversation est assez simple. Une fois celle-ci
affichée sur votre écran, tapez simplement votre réponse dans la fenêtre
d'édition située en-dessous des messages et appuyez sur le bouton
Répondre.

Il est possible de supprimer une conversation de votre liste. Pour cela,
sélectionnez la conversation dans le menu de gauche et cliquez sur le x
dans le coin supérieur droit. Bloquer une conversation a pour effets de
la supprimer de votre liste de conversations et d'arrêter de recevoir de
nouvelles réponses. Notez que les autres participants peuvent continuer
à se répondre dans la conversation – c'est juste que vous ne les
recevrez pas.

C'est tout ce qu'il y a à dire à propos des conversations. Vous êtes
pratiquement au bout de ce tutoriel ! Il est temps de lire la dernière
partie !

[Cinquième partie - Commencez à partager
!](5-partage.html) | [7ème
partie - Dernière ligne
droite](7-fin.html)

### Ressources utiles

-   [Base de code](http://github.com/diaspora/diaspora/)
-   [Documentation](https://wiki.diasporafoundation.org/)
-   [Trouver et signaler des
    bugs](http://github.com/diaspora/diaspora/issues)
-   [IRC - Général](http://webchat.freenode.net/?channels=diaspora)
-   [IRC -
    Développement](http://webchat.freenode.net/?channels=diaspora-dev)
-   [Liste - Général](http://groups.google.com/group/diaspora-discuss)
-   [Liste - Développement](http://groups.google.com/group/diaspora-dev)

[![Licence Creative
Commons](images/cc_by.png)](http://creativecommons.org/licenses/by/3.0/)
[diasporafoundation.org](https://diasporafoundation.org/) est protégé
sous licence [Licence publique Creative Commons Attribution 3.0 non
transposée](http://creativecommons.org/licenses/by/3.0/)

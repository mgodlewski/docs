# Tutoriel pour gérer ses Rendez-Vous avec Nextcloud

## Introduction

[Nextcloud](https://nextcloud.com) est une « plateforme de productivité auto-hébergée ». Elle permet notamment de stocker, synchroniser et partager des fichiers, mais aussi gérer par exemple ses agendas et contacts. Les fonctionnalités sont extensibles avec des « applications », par exemple pour éditer directement des fichiers, faire de la discussion en temps réel, ou - dans le cas qui nous intéresse - **permettre à des utilisateur⋅ices de prendre des rendez-vous !**

Framasoft propose donc une instance de Nextcloud à l’adresse <https://rdv-medecins.framasoft.org/> pour les personnes que ces fonctionnalités intéressent.

## Inscription

Rendez-vous sur [https://rdv-medecins.framasoft.org/](https://rdv-medecins.framasoft.org/settings/users) et cliquez sur « S’enregistrer ». Ensuite, entrez une adresse email pour confirmer votre inscription. Vous allez recevoir un email avec un lien de confirmation. En y accédant, vous pourrez définir votre nom d’utilisateur (celui-ci ne pourra pas être changé par la suite) et votre mot de passe. Enfin, connectez-vous au service.

## Interface

La barre du haut vous permet de naviguer entre les applications. Pour configurer les rendez-vous, se sont les deux icônes situées sur la gauche de la barre qui nous intéressent : \
Rendez-vous et Agenda

Pour information, tout à droite se trouvent les paramètres, où vous pourrez changer vos informations personnelles de compte (votre mail par exemple).

## Mise en place

### Créer un agenda dédié

Lors de votre première connexion à la plateforme, vous devriez arriver sur l'application « Agenda », où vos événements seront affichés. Cette interface permet d'afficher les événements en vue Mensuelle, Hebdomadaire et Journalière.

Pour commencer, créons un nouvel agenda pour stocker spécifiquement les événements professionnels. Cliquez sur « Nouvel agenda », choisissez « Nouvel agenda » et donnez-lui un nom, ici « Rendez-vous ». Appuyez sur la touche « Entrée » ou bien cliquez sur la flèche pour valider.

![Sélection_001-1.png](images/Sélection_001-1.png)

![Sélection_001.png](images/Sélection_001.png)

![Sélection_002.png](images/Sélection_002.png)

Ceci étant fait, ouvrez l’application de rendez-vous pour la paramétrer et définir vos créneaux.

![Sélection_002-2.png](images/Sélection_002-2.png)

### Configurer l'application de rendez-vous

Commencez par sélectionner l’agenda précédemment créé.

Pour ce faire, dans la colonne de gauche, cliquez sur les 3 points à droite de l’intitulé « Select Calendar » et sélectionnez votre agenda « Rendez-vous ».

![Sélection_003-1.png](images/Sélection_003-1.png)

![Sélection_004.png)](images/Sélection_004.png)

Cliquez ensuite sur « Paramètres » (en bas à gauche) afin de compléter vos coordonnées, sans oublier de valider chaque champs en cliquant sur la flèche.

![Sélection_006.png](images/Sélection_006.png)

L’aperçu de la page publique se met alors à jour.

![Sélection_007.png](images/Sélection_007.png)

Votre application est configurée, nous allons maintenant ajoutez des créneaux de rendez-vous disponibles.

### Ajouter ses créneaux de rendez-vous

Cliquez sur « Ajouter des rendez-vous »

![Sélection_009.png](images/Sélection_009.png)

Dans la fenêtre « Schedule Generator » qui s’ouvre, sélectionnez la semaine que vous souhaitez modifier ainsi que la durée d’un rendez-vous. Cliquez ensuite sur « Démarrer ».

![Sélection_010.png](images/Sélection_010.png)

Afin d’ajouter des rendez-vous pour une journée, cliquez sur les trois points à côté de la date voulue puis entrez le nombre de créneaux disponibles à la prise de rendez-vous et validez avec la touche « Entrée ».

![Sélection_011.png](images/Sélection_011.png)

Faites glisser les différents créneaux afin de les placer au bons endroits sur la grille horaire.

![Sélection_012.png](images/Sélection_012.png)

Dans le cas ou les horaires sont les mêmes sur plusieurs jours, vous pouvez copier ce que vous venez de paramétrer en cliquant sur les 3 points puis « Copy to Next ».

![Sélection_013.png](images/Sélection_013.png)

Répétez les opérations précédentes pour compléter les créneaux de la semaine.

Une fois cela fait, vous pouvez ajouter ces créneaux au calendrier et les rendre disponible pour la prise de rendez-vous en cliquant sur « Ajouter au calendrier ».

![Sélection_014.png](images/Sélection_014.png)

### Publier l'adresse à diffuser

Les créneaux disponibles sont paramétrés, il nous reste à rendre la page de prise de rendez-vous disponible et à la diffuser.

Pour ce faire, dans la colonne de gauche, cliquez sur les 3 points à droite de l’intitulé « Page publique [Désactivée] » et sélectionnez « Partager en ligne ».

![Sélection_015.png](images/Sélection_015.png)

### Partager l'agenda

Vous pouvez alors récupérer l’adresse de votre formulaire de rendez-vous en cliquant, au dessus de l’aperçu, sur « Copier le lien public ».

Il vous restera à partager le lien obtenu en le collant dans un mail ou sur tout autre moyen de communication pour donner accès à la page de prise de rendez-vous à vos patients.

![Sélection_016.png](images/Sélection_016.png)

Lorsqu’un rendez-vous est demandé par un patient, il est automatiquement visible dans l’agenda. Il suffit de cliquer sur le rendez-vous en question puis sur « Plus » pour voir les coordonnées de la personne (mail et numéro de téléphone).

![Sélection_017.png](images/Sélection_017.png)

![Sélection_018.png](images/Sélection_018.png)

## Synchronisation

Si vous souhaitez synchroniser votre agenda avec votre téléphone ou sur votre ordinateur, suivez les guides ci-dessous :

#### Bureau

* [Thunderbird](https://docs.framasoft.org/fr/agenda/Synchronisation/Thunderbird.html) (PC, MacOS, GNU/Linux, \*BSD)
* [Apple Calendar](https://docs.framasoft.org/fr/agenda/Synchronisation/ical.html) (MacOS X)
* [Apple Contacts](https://docs.framasoft.org/fr/agenda/Synchronisation/icard.html) (MacOS X)
* [Gnome](https://docs.framasoft.org/fr/agenda/Synchronisation/gnome.html) avec Evolution, Agenda et Contacts (GNU/Linux, \*BSD)
* [KOrganizer](https://docs.framasoft.org/fr/agenda/Synchronisation/kde.html) (GNU/Linux)

#### Mobile

* [DAVx⁵](https://docs.framasoft.org/fr/agenda/Synchronisation/Android.html) (Android)
* [iOS](https://docs.framasoft.org/fr/agenda/Synchronisation/iOS.html) (paramètres internes)
* [Windows Phone](https://docs.framasoft.org/fr/agenda/Synchronisation/WindowsPhone.html) (paramètres internes)
* [Ubuntu Touch](https://docs.framasoft.org/fr/agenda/Synchronisation/UbuntuTouch.html) (paramètres internes)
* [Jolla/SailFish OS](https://docs.framasoft.org/fr/agenda/Synchronisation/Jolla.md) (paramètres internes)

Cf. <https://docs.framasoft.org/fr/agenda/#synchronisation-avec-un-client>

## Plus d'aide

Si vous rencontrez des difficultés dans l’utilisation de l’application Rdv-Médecins, vous pouvez trouver de l’aide sur le forum [Framacolibri à la rubrique « Entraide »](https://framacolibri.org/c/entraide/53).

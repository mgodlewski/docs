# Déframasoftiser Framacalc

Afin de [déframasoftiser Internet](https://framablog.org/2019/09/24/deframasoftisons-internet/) voici comment exporter vos données depuis nos services.

## Export

### Export du fichier

Pour exporter vos données vous devez&nbsp;:

  1. cliquer sur l'icône «&nbsp;disquette&nbsp;» :
    ![image disquette sauvegarde framacalc](images/calc_sauvegarde_icone.png)
  * sélectionner le(s) format(s) dans lequel vous souhaitez exporter les données
  * enregistrer le fichier sur votre ordinateur

### Export alternatif

Si l'instance ethercalc ne prend pas encharge l'import de fichiers `ODS`, `HTML`, `CSV` ou `Excel`, vous pouvez copier puis coller les données du calc. Pour cela  il faut utiliser le presse papier avec le format SocialCalc. Le procédé est un peu laborieux :

  1. Sélectionner les cellules
  2. Cliquer sur l’icône **Copier**
  3. Aller dans l’onglet **Presse-papier**
  4. Choisir **Format de sauvegarde de SocialCalc**
  5. Sélectionner tout le texte qui s’affiche dans le cadre en dessous (ce qui commence par **version :…**)
  6. Le copier `CTRL+C`
  7. Aller dans la feuille de destination
  8. Choisir la cellule de destination
  9. Aller dans l’onglet **Presse-papier** (chaque feuille a son presse-papier)
  10. Choisir **Format de sauvegarde de SocialCalc**
  11. Coller le texte `CTRL+V`
  12. Cliquer sur **Copier le presse-papier de SocialCalc avec ceci** (afin de le mettre dans le presse papier de la feuille de destination)
  13. Cliquer sur **Édition**
  14. Cliquer sur l’icône **Coller**

## Import

Sur le site alternatif à Framacalc vous devez glisser/déposer le fichier [précédemment téléchargé](#export-du-fichier) dans la zone prévue à cet effet.

## Supprimer ses données

La création de calc étant anonyme, il est impossible de proposer la suppression des données, sinon n’importe qui pourrait supprimer n’importe quel calc. Cependant les calcs sont supprimés au bout d’un an d’inactivité (aucun accès, aucune modification), afin d’éviter de faire grossir indéfiniment notre base de données. En attendant vous pouvez sélectionner toutes les cellules et supprimer le contenu. Attention cependant, ceci ne supprimera pas le contenu de l’historique (les données seront toujours disponibles).

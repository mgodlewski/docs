# Articles

## Sauvegardez votre premier article

La fonctionnalité principale de wallabag est de sauvegarder des articles.
Vous avez plusieurs manières de le faire.

<p class="alert alert-info">
    Un guide de démarrage s'affichera dans l'application jusqu'à ce que vous
    enregistriez votre premier article.
</p>

### En utilisant le bookmarklet

Sur la page `Aide`, vous avez un onglet `Bookmarklet`. Glissez/déposez le lien `bag it!`
dans votre barre de favoris de votre navigateur.

Maintennat, à chaque fois que vous lisez un article et que vous souhaitez le sauvegarder,
cliquez sur le lien `bag it!` dans votre barre de favoris. L'article est enregistré.

### En utilisant le formulaire classique

Dans la barre haut de wallabag, vous avez trois icônes. Avec la première icône,
un signe plus, vous pouvez facilement ajouter un nouvel article.

![Barre supérieure](../images/topbar.png)

Cliquez dessus pour afficher un nouveau champ, collez-y l'URL de l'article et appuyez
sur la touche `Entrée`. L'article est enregistré.

### En utilisant l'extension de votre navigateur

#### Firefox

Vous pouvez télécharger [l'extension Firefox ici](https://addons.mozilla.org/firefox/addon/wallabag-v2/).

#### Chrome

Vous pouvez télécharger [l'extension Chrome ici](https://chrome.google.com/webstore/detail/wallabagger/gbmgphmejlcoihgedabhgjdkcahacjlj?hl=fr).

### En utilisant l'application de votre smartphone

#### Android

Vous pouvez télécharger [l'application Android ici](https://play.google.com/store/apps/details?id=fr.gaulupeau.apps.InThePoche).

#### Windows Phone

Vous pouvez télécharger [l'application Windows Phone ici](https://www.microsoft.com/store/apps/9nblggh5x3p6).

## Téléchargez vos articles

Vous pouvez télécharger chaque article dans plusieurs formats : ePUB, MOBI, PDF, XML, JSON, CSV.

Lorsque vous lisez un article, cliquez sur cette icône dans la barre latérale :

![Télécharger l'article](../images/download_article.png)

Vous pouvez aussi télécharger une catégorie (non lus, favoris, lus) dans ces formats.
Par exemple, dans la vue **Non lus**, cliquez sur cette icône dans la barre supérieure :

![Télécharger l'article](../images/download_articles.png)

## Partagez vos articles

Quand vous lisez un article, vous pouvez le partager. Cliquez sur le bouton de partage :

![Partager un article](../images/share.png)

Vous pouvez maintenant le partager :

- avec une URL publique (vous obtiendrez une vue allégée de l'article)
- avec un tweet
- dans votre Shaarli
- avec un message dans Diaspora*
- sur Carrot
- avec un email

## Annotez vos articles

Sur chaque article que vous lisez, vous pouvez écrire des annotations.
Puisqu'une image vaut mieux qu'un long discours,
voici ce que ça donne.

Sélectionnez la zone du texte que vous souhaitez annoter et cliquez sur le crayon :

![Sélectionnez votre texte](../images/annotations_1.png)

Ensuite, écrivez votre annotation :

![Écrivez votre annotation](../images/annotations_2.png)

Le texte est maintenant surligné et vous pouvez lire le annotation en le survolant avec votre souris.

![Lisez votre annotation](../images/annotations_3.png)

Vous pouvez créer autant de annotations que vous le souhaitez.

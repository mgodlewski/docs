# Le cas des polygones

Pourquoi traiter les polygones à part, il ne s'agit que d'une ligne fermée ? Un polygone est en réalité bien plus qu'un ligne fermée. Cette ligne sépare l'**intérieur du polygone** de son extérieur, ceci est important car uMap peut réagir à un clic à l'intérieur du polygone. De plus un polygone être troué, il est alors défini par plusieurs lignes.

## Ce que nous allons apprendre

*  Créer un polygone et le modifier
*  Styliser un polygone : remplissage et contour(s)
*  Associer une URL à un polygone

## Procédons par étapes

### 1. Créer un polygone

Revenons à la carte de nos vacances à Crozon. Un jour de beau temps nous louons un dériveur et naviguons dans la zone définie par le club nautique. Ajoutons cette zone à la carte.

![](images/umap_edit_polygon.png) Le bouton **Dessiner un polygone** permet de tracer le périmètre d'un polygone point par point, et de le terminer en cliquant à nouveau sur le dernier point comme pour le tracé d'une ligne. Une différence toutefois : dès le troisième point l'intérieur du polygone est coloré.

#### Propriétés d'un polygone

![](images/proprietes_polygones.png)

La liste des propriétés d'un polygone est assez longue. Les propriétés de la moitié supérieure du menu s'appliquent au périmètre du polygone, et sont identiques aux propriétés s'appliquant aux lignes. Le moitié inférieure concerne le remplissage du polygone. Noter :

*  les options **trait** et **remplissage** permettent de ne pas afficher le périmètre ou l'intérieur du polygone : si aucun de ces deux éléments est affiché le polygone est invisible.
*  la **couleur du remplissage** est par défaut celle du trait, mais peut être modifiée.
*  une faible **opacité du remplissage** permet de voir le fond de carte *sous* le polygone.

#### Trouer un polygone

Il est parfois utile de créer un ou plusieurs trous dans un polygone, par exemple pour dessiner une clairière dans une forêt ou un île au milieu d'un étang.

![](images/polygone_trou.jpg)

Vous pouvez créer un polygone avec un ou plusieurs trous en cliquant sur l'option **Ajouter un tracé intérieur** lorsque vous sélectionnez un polygone en mode édition.

Le premier point du *périmètre intérieur* est créé directement là où vous avez cliqué avant de choisir **Ajouter un tracé intérieur**.

Notez que les propriétés de périmètre d'un polygone s'appliquent à tous les périmètres - extérieurs et intérieurs.

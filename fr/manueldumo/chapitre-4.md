# Réseaux sociaux et hébergement

Un hébergement web est d'abord une prestation de service. Il s'agit de mettre à disposition  des serveurs afin que les utilisateurs puissent y stocker des données accessibles en tout temps (tant que le serveur fonctionne). La première forme d'hébergement couramment rencontrée est l'hébergement de site web&nbsp;: un serveur doté d'une configuration spécifique (serveur HTTP, système de gestion de base de données, etc.) permet à l'utilisateur d'ouvrir un compte, et stocker des fichiers permettant de créer un site publiquement accessible. Par exemple, le premier site web était hébergé sur un serveur du CERN (où furent inventées les fonctions hypertexte du World Wide Web) à la fin des années 1990[^12].

Mais aujourd'hui, la transformation des technologies web et l'accroissement de l'offre ont fait de l'hébergement un vaste marché économique. Des entreprises font héberger leurs données sensibles, car il est parfois plus intéressant de ne pas s'occuper soi-même de la sécurité de ses données et laisser le soin à d'autres professionnels de le faire. Des hébergeurs de sites web professionnels font payer des individus ou des entreprises pour monter leurs blogs ou leur vitrines de vente en ligne. Certains hébergements sont spécialisés dans le stockage de données à distances, d'autres sont spécialisés en plate-forme de blog, d'autres encore dans la gestion de base de données distantes, etc. Certaines formes d'hébergement sont gratuites ou offrent des fonctionnalités et en font payer d'autres (ce qu'on appelle les offres premium). Enfin, des hébergeurs libres et associatifs existent et, moyennant une adhésion,  proposent des services web plus ouverts.

Si vous voulez avoir votre profil sur un réseau social, héberger vos données, ouvrir votre blog personnel, partager vos photos et autres documents, synchroniser vos contacts ou vos notes personnelles tout en les partageant ou non… il est assez difficile de s'y retrouver dans toute l'offre disponible. En fait, il est surtout difficile de ne pas céder aux sirènes des solutions gratuites proposées par des grandes firmes dont on peut questionner les véritables intentions. Nous allons tenter dans ce chapitre d'y voir plus clair.



## Connaître mes réseaux sociaux

Les services de réseaux sociaux, tout comme les services de messagerie (cf. le chapitre 3), sont des services d'hébergement. Dans le cas d'un hébergement de messagerie, vos courriels  sont envoyés et copiés sur un serveur, relayés sur un autre serveur de réception et sur le terminal de votre correspondant lorsqu'il se connecte. Sur un service de réseau social classique (car nous verrons qu'il existe des alternatives) vous envoyez et copiez des contenus sur un serveur et ces contenus seront accessibles pour qui se connecte au même serveur.

Il existe plusieurs sortes de services de réseaux sociaux&nbsp;: Facebook, Google+ et LinkedIn sont des réseaux relationnels, Instagram un réseau pour photos et images, Youtube et Periscope sont spécialisés dans la vidéo… Tous ces services ont en commun le fait de centraliser les données et les connexions de manière à rendre captifs les internautes et générer une économie de l'attention en insérant des publicités, en analysant les comportements et en investissant par ailleurs sur des produits en lien avec ces comportements (comme par exemple des applications pour mobile ou des voitures sans chauffeur).

<div class="alert alert-info">
<p><img src="images/encarts/cloche.png" style="margin-right:10px;"><strong>Préférez des réseaux décentralisés</strong></p>

Le problème de la centralisation des réseaux sociaux, c'est la possibilité que leur accès soit compromis. Ce fut le cas par exemple en Égypte lors des manifestations populaires en hiver 2011 contre le gouvernement de Hosni Moubarak&nbsp;: les accès aux réseaux sociaux furent coupés par les autorités de manière à empêcher les relais d'appels à manifestations. Alors que les réseaux sociaux peuvent être d'excellents outils démocratiques, la centralisation des accès en est le talon d'Achille.

</div>

Un autre souci lié à l'usage de certains réseaux sociaux est la surveillance  systématique des échanges. Si cette surveillance ne s'apparentait qu'à une analyse des  contenus dans un but purement commercial, on pourrait à la rigueur considérer que les utilisateurs agissent en connaissance de cause et que, si leurs intimités numériques viennent à devenir publiques ou prises en otage par piratage, c'est leur responsabilité qui serait tout autant en jeu que celle du fournisseur de service. Hélas, ce n'est pas le cas. Comme le rapportent régulièrement les enquêtes journalistiques, des programmes de surveillance scannent *tous* les échanges, y compris privés,  en vue d'identifier ceux qui présentent éventuellement des risques sécuritaires&nbsp;: c'est le cas notamment de Facebook, pour qui fut identifié en 2012 un programme interne de surveillance (c'est-à-dire qui relève de la propre initiative de Facebook). Officiellement ce programme servait à identifier les risques terroristes, les réseaux pédophiles, etc., de manière à redorer le blason de Facebook auprès des autorités. Mais quelle société démocratique est prête à voir tout son courrier ouvert et lu au nom de la surveillance sécuritaire, pour sauvegarder les intérêts d'une firme privée ou même pour la raison d'État&nbsp;?

Si vous désirez utiliser un service de réseau social, posez-vous donc la question de savoir ce que vous êtes prêt à sacrifier pour cela. Si la démocratie, la liberté d'expression et votre intimité sont importants à vos yeux, il est vivement encouragé d'étudier quelques alternatives aux réseaux centralisés. En voici quelques unes.



### Diaspora&lowast;

Que faut-il pour qu'un réseau ne soit jamais coupé&nbsp;? Il faut que chacun de ses nœuds soit relié à tous les autres et que chaque nœud fasse passer l'information indifféremment vers l'un ou l'autre de ses semblables (ses pairs). Imaginons qu'un logiciel de réseau social soit installé sur chacun de ces nœuds&nbsp;: vous obtenez Diaspora&lowast;, c'est-à-dire un réseau social constitué de multiples instances interconnectées. 

Vous vous inscrivez sur une instance de votre choix et tous les membres des autres instances peuvent lire votre billet (si vous l'avez rendu public). Chaque instance peut avoir des conditions d'utilisation différentes, mais quoi qu'il en soit, vous pouvez communiquer entre tous les membres du réseau.

Ainsi Diaspora&lowast; est une alternative sérieuse et complète par rapport à un service comme Facebook. Par ailleurs des passerelles permettent de poster un billet sur Diaspora&lowast; et de le relayer automatiquement sur Facebook ou Twitter. Concernant les profils, ceux-ci sont chiffrés&nbsp;: faire d'un autre abonné un «&nbsp;ami&nbsp;» consiste à lui fournir une clé de chiffrement PGP (cf. le chapitre 5) lui permettant de déchiffrer votre profil privé ou vos messages privés. En d'autres termes, Diaspora&lowast; est un réseau social libre, décentralisé et sécurisé. Pour choisir un nœud (*pod*) Diaspora où vous pouvez vous inscrire et commencer à partager des contenus, rendez-vous sur la [liste Poduptime](https://podupti.me/). Par exemple, le *pod* maintenu par l'association Framasoft se nomme Framasphère, accessible sur [framasphere.org](https://framasphere.org/) et regroupe beaucoup d'utilisateurs. 


![Framasphère est un nœud Diaspora&lowast;](images/framasphere_screen.png)


### Movim

Movim est un réseau social décentralisé basé sur les mêmes principes que Diaspora&lowast; mais utilisant une technologie différente&nbsp;: le protocole XMPP (nous en parlons dans le chapitre 3). La force de Movim est d'avoir réussi à donner une dimension web à un protocole de messagerie. Ainsi, si vous disposez d'un compte Jabber/XMPP (très facile d'en créer un&nbsp;!), vous pouvez aussi vous connecter à Movim en utilisant les mêmes identifiants. Une autre solution consiste à choisir un nœud sur le site [movim.eu](https://movim.eu/).

![Un mur sur Movim](images/movim-screen.png)


### Seenthis

Seenthis (accessible sur [seenthis.net](https://seenthis.net)) est un service de microblogging, c'est à dire qu'il permet des publications courtes ou très courtes, mais néanmoins plus longues que ce que permet Twitter. Seenthis se revendique être une plate-forme de *short-blogging*, spécialisée dans la publication de textes très courts, de liens, d'images avec une légende, etc. Chaque billet peut aussi être commenté par les lecteurs. Par ailleurs, Seenthis ne vise pas à remplacer Twitter&nbsp;: des ponts existent entre les deux services et l'on peut émettre vers Twitter tout en postant un billet sur Seenthis. Là où Seenthis constitue une alternative, c'est pour deux raisons&nbsp;:

1. Le [code source est disponible](https://github.com/seenthis/seenthis), ce qui implique que le logiciel peut être installé ailleurs et être fréquenté par une autre communauté. Ainsi, par exemple Les Amis du Monde Diplomatique ont une plate-forme Seenthis, nommée [Zinc](http://zinc.mondediplo.net/), qui leur est dédiée (mais tout le monde peut s'y inscrire).  Néanmoins, Seenthis n'est pas un réseau décentralisé comme Diaspora&lowast; car il n'y a pas de correspondance entre différentes instances de Seenthis. Ainsi, une instance de Seenthis reste un réseau centralisé pour la communauté qui la fréquente.
2. Les [conditions d'utilisation de Seenthis](https://seenthis.net/fran%C3%A7ais/mentions/article/propri%C3%A9t%C3%A9-intellectuelle) (sur [seenthis.net](https://seenthis.net)) engagent le service à ne revendiquer aucun droit sur les billets publiés qui restent propriété de leurs auteurs, libres de les éditer ou les récupérer par la suite ou encore d'y apposer une licence libre. 

![Auteurs et thèmes suivis sur Seenthis](images/seenthis-screen.png)



### Mastodon

Enfin, il faut signaler [Mastodon](https://joinmastodon.org/), qui est à la fois un réseau social et une plate-forme de micro-blogage. Décentralisé à l'image de Diaspora*, il permet d'écrire des mini-billets (de 500 caractères maximum). Le réseau Mastodon est constitué de multiples instances, dont les inscriptions sont ouvertes, d'autres qui ne le sont pas. Chaque instance propose ses propres conditions d'utilisation, dont il est conseillé de prendre connaissance avant de s'inscrire. Une fois connecté, l'utilisateur peut choisir différents canaux~: ses propres abonnements, le canal de l'instance locale, le canal général (transversal). Des applications mobiles pour Mastodon existent, tels Tusky pour Android et Amaroq pour iOS.

En 2017, soit à peine un an après sa création, Mastodon regroupe des milliers d'utilisateurs, dont beaucoup ont déserté Twitter, préférant migrer vers cette solution libre. L'originalité de Mastodon est de permettre la naissance d'instances dont on choisi le degré d'ouverture au reste du réseau. Par exemple, une entreprise peut très bien installer Mastodon uniquement pour un usage interne, ou bien ouvrir son instance mais n'autoriser les inscriptions que pour ses membres.

On peut noter que la DINSIC (la Direction interministérielle du numérique et du système d'information et de communication) a ouvert [sa propre instance Mastodon](https://mastodon.etalab.gouv.fr), destinée aux agents publics de l'État Français, un cas d'usage assez rare pour être souligné. Framasoft a ouvert l'une des plus importantes instances francophones, nommée [Framapiaf](https://framapiaf.org), de même pour le magazine [Numérama](https://social.numerama.com),  [La Quadrature du net](https://mamot.fr/), et bien d'autres.

![Instance Framapiaf pour Mastodon](images/mastodon.png)


## Le cloud


Le *cloud computing* ou l'informatique en nuage consiste à utiliser les capacités de serveurs distants en passant par le réseau. Le principe est déjà très ancien&nbsp;! C'était le but des premiers ordinateurs que de servir de ressources centrales. Les gros ordinateurs *mainframe* qui furent construits dès les années 1950 accomplirent essentiellement ce rôle, de plus en plus avec le réseau Internet naissant à la fin des années 1960, mais en perte de vitesse avec l'apparition de l'informatique personnelle dans les années 1980. Ces ordinateurs étaient dotés d'une particularité&nbsp;: ils avaient un système d'exploitation à *temps partagé*, c'est-à-dire que plusieurs utilisateurs (notamment, à cette époque, des instituts de recherche, des banques, des ingénieurs aéronautiques…) pouvaient se connecter simultanément et effectuer des opérations en profitant du temps de calcul disponible. C'est l'une des caractéristiques qui a fait le succès du système Multics (Multiplexed Information and Computing Service), qui deviendra Unix plus tard. 

![IBM System/360 au Computer History Museum. Wikipedia Commons. Erik Pitti. CC-By](images/IBM_System360_Mainframe.jpg)



Aujourd'hui, il existe un *cloud computing* destiné au grand public[^13]. Il consiste essentiellement à&nbsp;:

* héberger des fichiers (documents, photos, vidéos…),
* utiliser des logiciels en ligne (traitement de texte, espaces collaboratifs…),
* synchroniser des bases de données (des contacts, agendas…).



## Enjeux de sécurité

Si vous choisissez de stocker des données à distance, que ce soit pour effectuer des sauvegardes ou pour partager des fichiers, cela implique d'évaluer le niveau de confiance que vous êtes prêt à accorder à un tiers. Dans le chapitre 5 nous expliquons qu'il est parfois souhaitable de chiffrer ses fichiers, notamment avec PGP. Lorsque vous stockez des fichiers sur l'ordinateur d'un tiers, vous avez tout intérêt à les chiffrer, au moins pour que personne d'autre que vous ne puisse les lire.

L'exemple du service Dropbox illustre bien le problème du stockage de données personnelles à grande échelle. En vrac&nbsp;:

* en janvier 2017, des utilisateurs s'aperçoivent que des fichiers supprimés depuis longtemps réapparaissent, ce qui sous-entend que même si l'utilisateur décide de supprimer des données, Dropbox les conserve[^14]&nbsp;;
* en été 2016, Dropbox demande à ses abonnés de changer de mot de passe&nbsp;: des pirates ont réussi à exploiter une faille connue depuis 2012 et 68&nbsp;millions de comptes (deux tiers des utilisateurs) ont ainsi vu leurs identifiants piratés[^15]&nbsp;;
* au printemps 2016, Dropbox sort une nouvelle version dont l'application locale   s'immisce dans le système d'exploitation des utilisateurs au risque de créer de graves failles de sécurité[^16].

Il faut considérer que les difficultés rencontrées par le service Dropbox ne sont pas extraordinaires. Leurs conséquences, en revanche, sont particulièrement problématiques&nbsp;: en créant un immense silo de données pour des millions d'utilisateurs, un tel service s'expose naturellement à un taux élevé de risque d'erreur et à moult convoitises. Dans ces conditions, centraliser toujours plus de données et créer un monopole n'est pas un facteur de confiance.

D'un autre côté, choisir un hébergeur associatif ou auto-héberger ses données implique aussi des risques (que l'association soit dissoute, que les mesures de sécurité ne soient pas d'un niveau suffisant, etc.) , mais ils seront d'autant moindres que la structure à laquelle on accorde sa confiance reste assez proche de ses utilisateurs pour leur garantir l'effort et le conseil. Quant à l'auto-hébergement, il est sage de prendre la mesure de ses propres compétences.


## Quels services choisir&nbsp;?

Avant de choisir un service dans les nuages, il faut réfléchir à l'usage que l'on souhaite en faire. S'il s'agit par exemple de prendre quelques notes, un service d'édition de documents en ligne ne sera pas pertinent. S'il s'agit de partager les dernières photos de famille avec les membres éloignés, on peut préférer un dépôt chiffré d'album photo à télécharger plutôt que leur affichage dans un réseau social pour lequel vos correspondants n'ont pas forcément de compte.

Dans le cadre de sa campagne *Dégooglisons Internet*, l'association Framasoft propose une foule de services en ligne. Le but est de démontrer que les logiciels libres sur lesquels sont basés ces services constituent autant d'alternatives sérieuses aux monopoles des services web. Les destinataires sont de deux ordres. D'abord le grand public, qui est ainsi initié à d'autres outils et invité à d'autres usages plus respectueux des données personnelles. Ensuite, les individus (les experts), les associations, les entreprises, qui sont invités à essaimer ces services pour le plus grand nombre.

Tel est l'objectif du mouvement impulsé par Framasoft nommé [CHATONS](https://chatons.org/), le Collectif des Hébergeurs Associatifs, Transparents, Ouverts, Neutres et Solidaires. Chaque «&nbsp;Chaton&nbsp;» est une association (ou éventuellement un groupement) qui propose des services ouverts et basés sur des logiciels libres, tout en respectant un modèle de charte et un manifeste visant à garantir une éthique respectueuse des intimités numériques.

Ainsi, avant de trouver un «&nbsp;Chaton&nbsp;» autour de chez vous, tester les outils proposés par Framasoft vous permettra de trouver un hébergement en ayant une idée plus précise de vos besoins. Rendez-vous donc sur le site de la campagne [Degooglisons-internet.org](https://degooglisons-internet.org/).


Bien sûr, il reste la question du coût. Alors que les grands monopoles proposent des solutions gratuites (avec un ajout de fonctionnalités payantes), il peut sembler difficile de payer pour une offre *a priori* similaire. Cependant, les organisations offrant des alternatives libres et éthiques, le font généralement sur des modèles économiques différents&nbsp;:

* organisme à but non lucratif, basé sur le bénévolat, le don et de faibles cotisations visant essentiellement à couvrir les frais d'infrastructures,
* petites sociétés utilisant des solutions de logiciels libres et contribuant au code source des logiciels&nbsp;: elles offrent généralement des tarifs tout à fait abordables pour les particuliers avec des garanties plus que suffisantes.

Par conséquent, en guise de choix, s'il faut éviter les offres gratuites offertes par les géants du web, il faut néanmoins avancer un peu d'argent (quelques euros par an, généralement), tel est le prix de la gratuité du libre&nbsp;: le soutien aux communautés qui les font vivre.



### Stocker et synchroniser mes fichiers, mes contacts, mon agenda

Plusieurs logiciels libres permettent de créer un service *cloud* permettant de stocker des données à distance et capable d'effectuer des tâches de synchronisation entre plusieurs machines (votre ordinateur, votre tablette, votre smartphone, etc.).

L'un des plus connus se nomme [Nextcloud](https://nextcloud.com/). Après l'ouverture d'un compte, vous pouvez télécharger un client sur votre machine, qui se chargera de synchroniser un dossier local avec un serveur distant&nbsp;: ce que vous mettez dans ce dossier sera stocké à distance, si vous modifiez le contenu à distance ou sur une autre machine disposant du même accès, votre dossier sera d'autant modifié. Nextcloud dispose d'une interface web, d'un client local, et d'une application smartphone.

Le service [Framadrive](https://framadrive.org/), de Framasoft, propose une instance gratuite pour des comptes limités à 2 Go de données&nbsp;: vous pouvez ainsi tester Nextcloud. Une autre instance, [Framagenda](https://framagenda.org), propose un service Nextcloud spécialement dédié à la gestion de ses contacts et agendas.

Si vous désirez utiliser un service plus conséquent, vous pouvez ouvrir un compte auprès d'un acteur comme par exemple l'association [La Mère Zaclys](https://www.zaclys.com/) ou l'entreprise [Indiehosters](https://indiehosters.net/page/home). Ce ne sont que deux exemples&nbsp;: vous pouvez trouver sur [Chatons.org](https://chatons.org/) une liste d'acteurs prêts à vous accueillir&nbsp;!


![Interface web de Nextcloud](images/nextcloud_interfaceweb.png)


### Pour correspondre et collaborer

Les services infonuagiques connaissent une grande popularité dans le domaine de la collaboration en ligne. Pour votre association, par exemple, il est intéressant de pouvoir disposer d'un service de sondage pour s'accorder sur la date de la prochaine assemblée générale, ou encore d'un tableau blanc où il est possible d'écrire à plusieurs un compte-rendu de manière à gagner du temps et ne pas se confondre dans de multiples échanges de courriel. Vous pouvez aussi avoir besoin d'envoyer à vos correspondants des fichiers encombrants comme vos dernières photos de vacances. De multiples applications dans les nuages sont ainsi disponibles et visent à faciliter le travail en commun et les échanges de données.

Là encore Framasoft, dans le cadre de la campagne [Degooglisons-internet.org](https://degooglisons-internet.org/), vous propose une trentaine de services différents, basés sur des solutions de logiciels libres, et même avec un système de chiffrement. Parmi ces services, vous trouverez certainement le logiciel qui répondra à votre besoin et vous pourrez en faire profiter vos correspondants. Citons-en quelques-uns en guise d'illustrations&nbsp;:

* [Framadate](https://framadate.org/)&nbsp;: pour ouvrir des sondages afin de convenir à plusieurs d'une date de rendez-vous, ou créer un petit sondage classique&nbsp;;
* [Framapic](https://framapic.org/)&nbsp;: pour envoyer de manière sécurisée des images ou un album d'images à vos correspondants&nbsp;;
* [Framapad](https://framapad.org/)&nbsp;: un traitement de texte où l'on peut collaborer en temps réel à plusieurs pour écrire un document avec, en prime, un module de discussion instantanée&nbsp;;
* [Framatalk](https://framatalk.org/)&nbsp;: un système sécurisé de conversation vidéo&nbsp;;
* [Framalistes](https://framalistes.org/)&nbsp;: pour créer et gérer une liste de discussion par courriel&nbsp;;
* [Framaforms](https://framaforms.org/)&nbsp;: pour réaliser des enquêtes en ligne&nbsp;;
* [Framadrop](https://framadrop.org/)&nbsp;: pour envoyer de lourds fichiers à vos correspondants&nbsp;;
* etc.

<div class="alert alert-info">
<p><img src="images/encarts/cloche.png" style="margin-right:10px;"><strong>Cherchez votre chaton</strong></p>

Le mouvement initié par le collectif <a href="https://chatons.org/">Chatons.org</a> a pour objectif d'essaimer ce type de service, sur la base d'une charte de confiance. Vous pouvez ainsi trouver un «&nbsp;Chaton&nbsp;» qui vous fournira ce dont vous avez besoin.

</div>

![Framadate, un service Framasoft](images/framadate.png)



## L'auto-hébergement

Nous ne saurions terminer cette partie sans mentionner la question de l'auto-hébergement de services. En effet, si vous disposez d'un serveur, qu'il soit chez vous ou loué chez un fournisseur, vous pouvez héberger vos propres solutions basées sur des logiciels libres. Pour vous y aider, chaque service Framasoft dispose d'une rubrique «&nbsp;cultivez votre jardin&nbsp;» qui décrit la manière d'installer les logiciels concernés. Cela suppose néanmoins un certain niveau d'expertise qu'il vous faut acquérir, bien que cela soit accessible assez facilement. Un autre prérequis est de pouvoir disposer d'un temps suffisant pour maintenir un serveur à jour et assez d'assurance pour en garantir la sécurité.

Des systèmes peuvent néanmoins vous aider à accomplir ces tâches apparemment fastidieuses. En voici deux&nbsp;:

* [Yunohost](https://yunohost.org/)&nbsp;: il permet de déployer plusieurs logiciels à la demande, héberger ainsi ses propres services et éventuellement inviter d'autres utilisateurs à en profiter&nbsp;;
* [Cozycloud](https://cozy.io/fr/)&nbsp;: il s'agit d'une solution intégrée de plusieurs logiciels déployés ensemble pour offrir un éventail de services personnels.

Quel que soit votre choix, entre héberger vos propres services ou utiliser ceux d'un tiers, ces solutions démontrent la grande malléabilité des logiciels libres qui permettent de décentraliser les offres et remodeler les chaînes de confiance entre les utilisateurs et les hébergeurs. 




[^12]: On peut se reporter au site internet du CERN (Conseil européen pour la recherche nucléaire,) qui contient une rubrique dédiée «&nbsp;[La naissance du web](https://home.cern/fr/topics/birth-web)&nbsp;».

[^13]: Une autre application moins connue, consiste à faire du temps partagé «&nbsp;à l'envers&nbsp;», c'est-à-dire profiter des capacités de calcul des ordinateurs personnels pour centraliser ensuite les résultats. C'est l'exemple de [BOINC](http://boinc.berkeley.edu/) (Berkeley Open Infrastructure for Network Computing), une plate-forme de calcul distribué&nbsp;: les utilisateurs installent sur leur ordinateur un petit programme qui tourne en tâche de fond lorsque l'ordinateur est peu ou pas utilisé. Cela crée un potentiel de puissance de calcul énorme qui peut alors être mis à disposition de la recherche dans différents domaines (médecine, astrophysique, mathématiques…).

[^14]: Voir Gabriele Porrometo, «&nbsp;[Dropbox justifie la soudaine réapparition de fichiers supprimés sans vraiment convaincre](http://www.numerama.com/tech/227760-dropbox-justifie-la-soudaine-reapparition-de-fichiers-supprimes-sans-vraiment-convaincre.html)&nbsp;», *Numerama*, 27/01/2017.

[^15]: Voir 	Guillaume Serries, «&nbsp;[Dropbox&nbsp;: 68 millions d'identifiants dans la nature, mais tout va bien](http://www.zdnet.fr/actualites/dropbox-68-millions-d-identifiants-dans-la-nature-mais-tout-va-bien-39841266.htm)&nbsp;», *ZDNet*, 31/08/2016.

[^16]: Voir Christophe Lagane, «&nbsp;[Le nouveau Dropbox, une menace pour la sécurité&nbsp;?](http://www.silicon.fr/le-nouveau-dropbox-une-menace-pour-la-securite-148684.html)&nbsp;», *Silicon.fr*, 30/05/2016.



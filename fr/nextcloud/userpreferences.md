Régler les préférences utilisateur
========================

En tant qu'utilisateur, vous pouvez gérer vos paramètres personnels.

Pour accéder à vos paramètres personnels :

  1. Cliquez sur votre nom d'utilisateur, situé dans le coin en haut à droite de votre instance Nextcloud.

  Le menu des paramètres personnels s'ouvre.

  ![](images/oc_personal_settings_dropdown.png)

  *Menu des paramètres personnels*

  2. Sélectionnez l'option *Personnel* du menu déroulant.

> **note**
>
> Si vous êtes administrateur, vous pouvez également gérer les utilisateurs et administrer le serveur. Ces liens ne sont pas affichés pour les utilisateurs qui ne sont pas administrateurs.

Les options affichées sur la page des paramètres personnels dépendent des applications activées par l'administrateur. Voici certaines des fonctionnalités que vous pourrez voir :

-   Le quota disponible et utilisé ;
-   Gérer votre image de profil;
-   Votre nom complet. Cela peut-être ce que vous voulez car il est distinct de votre identifiant Nextcloud (qui est unique et ne peut être modifié) ;
-   Votre adresse mail ;
-   Les différents groupes auxquels vous appartenez;
-   Votre mot de passe
-   [L'authentification en deux étapes](user_2fa.html)
-   Régler les préférences utilisateur
-   La langue utilisée pour l'interface de Nextcloud ;
-   Les liens vers les différentes versions (bureau ou mobile) de l'application ;
-   Les paramètres du flux d'activité et des notifications ;
-   Le répertoire par défaut où sont enregistrés les nouveaux documents ;
-   Votre identifiant pour le partage fédéré.
-   Les liens pour le partage sur les réseaux sociaux ;
-   La version de Nextcloud.

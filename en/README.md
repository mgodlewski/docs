# Documentations

## Framasoft free services

<div>
  <div class="col-md-3 col-sm-6">
    <a href="dolomon/index.html" class="btn btn-lg btn-block btn-default">
      <p><i class="fa fa-2x fa-hand-pointer-o"></p>
      <p><b class="violet">Frama</b><b class="vert">clic</b></p>
      <p><b>Dolomon</b></p>
    </a>
  </div>
  <div class="col-md-3 col-sm-6">
    <a href="framadate/index.html" class="btn btn-lg btn-block btn-default">
      <p><i class="fa fa-2x fa-calendar-check-o"></p>
      <p><b class="violet">Frama</b><b class="vert">date</b></p>
      <p aria-hidden="true"><br></p>
    </a>
  </div>
  <div class="col-md-3 col-sm-6">
    <a href="lufi/index.html" class="btn btn-lg btn-block btn-default">
      <p><i class="fa fa-2x fa-send"></p>
      <p><b class="violet">Frama</b><b class="vert">drop</b></p>
      <p><b>Lufi</b></p>
    </a>
  </div>
  <div class="col-md-3 col-sm-6">
    <a href="lstu/index.html" class="btn btn-lg btn-block btn-default">
      <p><i class="fa fa-2x fa-link"></p>
      <p><b class="violet">Frama</b><b class="vert">link</b></p>
      <p><b>Lstu</b></p>
    </a>
  </div>
  <div class="col-md-3 col-sm-6">
    <a href="mastodon/index.html" class="btn btn-lg btn-block btn-default">
      <p><i class="fa fa-2x fa-mastodon"></p>
      <p><b class="violet">Frama</b><b class="vert">piaf</b></p>
      <p><b>Mastodon</b></p>
    </a>
  </div>
  <div class="col-md-3 col-sm-6">
    <a href="lutim/index.html" class="btn btn-lg btn-block btn-default">
      <p><i class="fa fa-2x fa-photo"></p>
      <p><b class="violet">Frama</b><b class="vert">pic</b></p>
      <p><b>Lutim</b></p>
    </a>
  </div>
  <div class="col-md-3 col-sm-6">
    <a href="diaspora/index.html" class="btn btn-lg btn-block btn-default">
      <p><i class="fa fa-2x fa-diaspora"></p>
      <p><b class="violet">Frama</b><b class="vert">sphère</b></p>
      <p><b>Diaspora*</b></p>
    </a>
  </div>
  <div class="col-md-3 col-sm-6">
    <a href="jitsimeet/index.html" class="btn btn-lg btn-block btn-default">
      <p><i class="fa fa-2x fa-video-camera"></p>
      <p><b class="violet">Frama</b><b class="vert">talk</b></p>
      <p><b>Jitsi Meet</b></p>
    </a>
  </div>
  <div class="col-md-3 col-sm-6">
    <a href="mattermost/index.html" class="btn btn-lg btn-block btn-default">
      <p><i class="fa fa-2x fa-comments-o"></p>
      <p><b class="violet">Frama</b><b class="vert">team</b></p>
      <b>Mattermost</b></p>
    </a>
  </div>
  <div class="col-md-3 col-sm-6">
    <a href="loomio/index.html" class="btn btn-lg btn-block btn-default">
      <p><i class="fa fa-2x fa-bullhorn"></p>
      <p><b class="violet">Frama</b><b class="vert">vox</b></p>
      <p><b>Loomio</b></p>
    </a>
  </div>
</div>

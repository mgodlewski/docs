# Framasite «&nbsp;Page&nbsp;»

[Framasite](https://frama.site/) est un service libre de création de sites web.

1. Créez votre site
2. Connectez-vous à son interface de gestion
3. Personnalisez et publiez vos pages web

![images framasites](images/noemie_screen.png)

## Présentation vidéo

<iframe width="560" height="315" sandbox="allow-same-origin allow-scripts" src="https://framatube.org/videos/embed/be26f331-de14-40fc-a9fd-fdc359f87101" frameborder="0" allowfullscreen></iframe>

<iframe width="560" height="315" sandbox="allow-same-origin allow-scripts" src="https://framatube.org/videos/embed/bc5b5ab6-b322-43c3-8881-0d4f17974ca6" frameborder="0" allowfullscreen></iframe>

## Pour aller plus loin&nbsp;:

- Utiliser [Framasite](https://frama.site/)
- [Prise en main](prise-en-main.html)
- [Modules](modules.html)
- [Site de démonstration](https://association-exemple.frama.site/)
- Un service proposé dans le cadre de la campagne [contributopia](https://contributopia.org/)

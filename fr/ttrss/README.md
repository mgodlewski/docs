# Framanews

[Framanews](https://framanews.org/) est un service gratuit proposé par le réseau Framasoft.

Vous disposez ainsi d’un lecteur de flux RSS en ligne, vous permettant d’être toujours au courant de l’actualité à partir des flux RSS de vos sites préférés !

Il s’agit d’une instance du Logiciel Libre [Tiny Tiny RSS](http://tt-rss.org/) (souvent abrégé ttrss).

![capture framanews](images/framanews-screenshot.png)

---

### Pour aller plus loin :

-   [Déframasoftiser Internet](deframasoftiser.html)
-   Tester [Framanews](https://framanews.org/)
-   [Foire Aux Questions](FAQ.md)
-   [Prise en mains](prise-en-main.md)
-   Le [site officiel de ttrss](http://tt-rss.org/) (à soutenir !)
-   [Installer ttrss](https://framacloud.org/fr/cultiver-son-jardin/ttrss.html) sur vos serveurs
-   [Dégooglisons Internet](http://degooglisons-internet.org/)
-   [Soutenir Framasoft](https://soutenir.framasoft.org)

# Votre profil utilisateur

Votre page de profil vous permet de paramétrer votre identité Framavox. Pour visiter votre page de profil, sélectionnez **Modifier mon profil** dans le menu utilisateur en haut à droite de la page.

## Mettre à jour vos paramètres personnels

Vous pouvez mettre à jour le nom, le nom d‎‎’utilisateur, l‎‎’adresse de courriel et les paramètres de langue associés à votre compte en renseignant les champs correspondants sur votre page de profil, et en cliquant sur **Mettre à jour le profil**.

## Afficher une photo pour votre profil

Quand vous créez un compte Framavox, vos initiales vont être l‎‎’image par défaut associée à votre profil. Vous pouvez mettre en ligne une photo de profil en cliquant sur **CHANGER DE PHOTO** dans [votre page de profil](https://framavox.org/profile), et en cliquant sur **Mettre à jour le profil**.

## Paramètres de langue

Framavox est accessible dans de nombreuses langues. Il va détecter automatiquement la langue de votre navigateur, mais vous pouvez choisir manuellement votre langue préférée en allant sur votre [page de profil](https://framavox.org/profile) et en sélectionnant votre langue depuis le menu déroulant **Langue**, et en cliquant sur **Mettre à jour le profil**.

## Nom d‎‎’utilisateur

Votre nom d‎‎’utilisateur est le nom que les autres participants vont utiliser pour vous mentionner ([@mention](comments.html#-mentioning-group-members)) dans les commentaires. Vous pouvez changer votre nom d‎‎’utilisateur en renseignant le champ **Identifiant utilisateur** sur votre [page de profil](your_user_profile.html#your-user-profile) et en cliquant sur **Mettre à jour le profil**.

## Changer votre mot de passe

Si vous êtes connecté à Framavox, vous pouvez changer votre mot de passe en allant sur votre page de profil et en cliquant sur **Réinitialiser mon mot de passe**.

Si vous êtes déconnecté, vous devez cliquer sur **RECEVOIR UN COURRIEL DE CONNEXION** à l'étape de demande du mot de passe lors de la connexion.

## Désactiver votre compte

Si vous n‎‎’êtes pas l‎‎’unique coordinateur d‎‎’un groupe vous pouvez désactiver votre compte en allant sur votre [page de profil](https://framavox.org/profile) et en cliquant sur **Désactiver votre compte…**. Si vous êtes le seul coordinateur d‎‎’un groupe, vous ne pourrez pas désactiver votre compte tant que vous n‎‎’aurez pas désigné un autre coordinateur de groupe, ou archivé le groupe.

Une fois que votre compte a été désactivé&nbsp;:

* vous ne serez plus listé comme membre d‎‎’un groupe
* les commentaires, propositions et discussions que vous avez faites resteront, mais votre nom sera supprimé
* vous ne recevrez plus de courriels concernant Loomio
* vous devrez [nous contacter](https://contact.framasoft.org) pour réactiver votre compte.

## Réactiver un compte

Pour réactiver un compte désactivé, [contactez nous](https://contact.framasoft.org).

## Supprimer un compte

Vous pouvez supprimer votre compte définitivement en allant dans [votre profil](https://framavox.org/profile) et en cliquant sur **SUPPRIMER LE COMPTE**.

## Quitter un groupe

<img class="image du menu de préférences" alt="Menu des options de groupe" src="images/options_dropdown.png" />

Si vous n‎‎’êtes pas le seul coordinateur d‎‎’un groupe, vous pouvez quitter le groupe en sélectionnant **Quitter le groupe** depuis le menu Options sur la page du groupe. Si vous êtes le seul coordinateur de votre groupe, vous ne pourrez pas le quitter tant que vous n‎‎’aurez pas désigné un autre coordinateur ou désactivé le groupe.

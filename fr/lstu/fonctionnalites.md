# Fonctionnalités

## Export/Import de la liste de liens

Les raccourcis créés dans un navigateur sont sauvegardés dans **ce navigateur seulement** : si vous en changez, vous n'aurez pas la liste de vos raccourcis.

Pour les retrouver dans un autre navigateur vous devez les exporter de celui-ci pour les importer dans le nouveau. Pour cela :

  * cliquez sur le bouton [Mes liens](https://frama.link/stats) sur la page https://frama.link/
  * cliquez sur **Exporter vos URL**
  * téléchargez le fichier `lstu_export.json` sur votre ordinateur
  * ouvrez le navigateur dans lequel vous souhaitez retrouver votre liste de raccourcis
  * ouvrez la page [Mes liens](https://frama.link/stats)
  * cliquez sur **Parcourir**
  * sélectionnez le fichier `lstu_export.json` précédemment téléchargé
  * cliquez sur **Importer des URL**

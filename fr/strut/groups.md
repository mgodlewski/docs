# Groupes

Framaslides vous permet de gérer vos contenus avec d'autres utilisateurs au sein de groupes.

## Création d'un groupe

Pour créer un groupe, rendez-vous sur la page de liste des groupes en cliquant sur l'icône de groupes dans la barre de navigation, puis choisissez « Créer un nouveau groupe ».

Vous devez tout d'abord choisir un nom pour le groupe. Ensuite, vous devez choisir les droits par défaut pour tout nouvel utilisateur. Vous avez le choix entre :

* **Lecture seule** : Le membre peut uniquement consulter les présentations, mais n'a pas le droit de les modifier.
* **Écriture** : Le membre peut modifier et créer de nouvelles présentations
* **Gestion des présentations** : Le membre peut choisir de marquer la présentation comme modèle
* **Gestion des membres** : Le membre peut approuver les requêtes à rejoindre le groupe d'autres membres, inviter d'autres membres et exclure des membres. Il peut aussi gérer les droits sur les membres
* **Administrateur** : Tous les droits référents, y compris renommer et supprimer le groupe.

Vous devez aussi choisir la méthode pour rejoindre votre groupe :

* **Ouvert** : Tout utilisateur peut rejoindre le groupe
* **Demande d'admission** : L'utilisateur doit demander l'autorisation à rejoindre le groupe. Un membre avec des droits peut l'y autoriser.
* **Mot de passe** : L'utilisateur doit saisir un mot de passe pour rejoindre le groupe
<!--* **Sur invitation uniquement** : Un membre du groupe doit inviter l'utilisateur pour qu'il puisse rejoindre le groupe-->
<!--* **Sur invitation et caché** : Semblable, mais le groupe n'est pas listé dans les groupes publics-->

Si vous avez choisi la méthode par mot de passe, il faut également le renseigner en dessous.

## Rejoindre un groupe

Pour rejoindre un groupe il faut le rechercher sur la page [Groupes publics](https://framaslides.org/groups) (**Liste des groupes** > onglet **Groupes publics**).

![image rejoindre un groupe](images/slides-rejoindre-groupe.png)

En fonction de la méthode choisie à [la création du groupe](#création-dun-groupe), les membres pourront voir les présentations partagées par le groupe dans [Présentations partagées avec mes groupes](https://framaslides.org/presentations/group/list) (**Voir mes présentations** > onglet **Présentations partagées avec mes groupes**).

## Gestion d'un groupe

La page de gestion d'un groupe permet d'éditer le groupe, de voir les membres et de modifier leurs droits d'accès, de voir les demandes d'ajout au groupe et d'inviter des personnes au groupe.

## Éditer un groupe

Éditer un groupe permet de redéfinir toutes les informations définies lors de sa création

## Partager une présentation avec un groupe

Sur la page de gestion d'une présentation, vous pouvez partager la présentation avec les groupes dont vous faites partie et sur lesquels vous avez les droits requis.

![Paramètres de la présentation](images/presentation_settings.png)

# Videos

Framaslides ne peut intégrer que deux types de vidéos : les vidéos depuis YouTube et les vidéos au format `webm`.

Pour intégrer une vidéo, cliquez sur le bouton « Vidéo » puis renseignez l'URL de la vidéo à intégrer, avant de cliquer sur le bouton « Insérer une vidéo ».

## Déplacer une vidéo

Il est assez difficile de déplacer une vidéo sans déclencher sa lecture. Pour cela, vous pouvez vous aider des flèches de déplacement qui apparaissent autour de la vidéo lorsqu'elle est sélectionnée. Afin de sélectionner la vidéo sans la lire, vous pouvez sélectionner une zone contenant la vidéo.

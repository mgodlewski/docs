# Déframasoftiser Framapic

Afin de [déframasoftiser Internet](https://framablog.org/2019/09/24/deframasoftisons-internet/) voici comment exporter vos données depuis nos services.

Le logiciel [lutim](https://framagit.org/fiat-tux/hat-softwares/lutim) utilisé pour Framapic peut être installé par d'autres (voir [la documentation](https://framacloud.org/fr/cultiver-son-jardin/lutim.html)), comme par exemple, chez un [CHATON](https://chatons.org/fr/find?title=&field_chaton_services_tid=65&field_chaton_type_tid=All).

<div class="alert-info alert">Les fichiers envoyés sont listés dans le navigateur utilisé pour l'envoi. Vous ne pouvez retrouver les fichiers envoyés depuis un autre navigateur. Les informations sont stockées en localStorage : si vous supprimez vos données localStorage, vous perdrez ces informations.</div>

## Export des données

Pour exporter vos données d'un navigateur vous devez&nbsp;:

  1. vous rendre dans l'onglet **[Mes images](https://framapic.org/myfiles)**
  * cliquer sur **Exporter les données localStorage**
  * enregistrer le fichier **data.json** sur votre ordinateur

## Importer des données

Après avoir téléchargé le fichier **data.json** sur votre ordinateur, vous pouvez l'importer dans un autre site utilisant le logiciel lutim (voir plus haut).

Pour ce faire, vous devez&nbsp;:

  1. vous rendre dans l'onglet **Mes Images**
  * cliquer sur **Importer des données localStorage**
  * récupérer votre fichier **data.json**
  * cliquer sur **Ouvrir**

Les fichiers seront alors listés sur le nouveau site. Ils restent cependant **hébergés** sur l'ancien site.

Cinquième partie - Commencez à partager !
-----------------------------------------

Maintenant que vous avez quelques contacts, il est temps de commencer à
partager du contenu avec eux.

Vous pouvez communiquer sur diaspora\* en partager un message de statut
dans des aspects ou même publiquement, ou en envoyant un message privé à
un ou plusieurs contacts mutuels. Nous nous concentrerons ici sur
l'utilisation de l'éditeur et du flux de messages pour publier et
commenter des messages de statut. Nous nous occuperons de l'envoi de
messages privés (ce qu'on appelle une "conversation") dans la [Partie
6](6-conversations.html).

## L'éditeur

Le partage de contenu sur diaspora\* se fait via l'éditeur qui est situé
en haut de la colonne centrale sur la plupart des pages de l'interface
de diaspora\*. Vous avez probablement déjà cliqué dessus et, si vous
l'avez fait, vous n'avez probablement pas pu vous empêcher de publier
quelque chose. Publier un message de statut est on ne peut plus simple.
Un peu plus loin dans ce tutoriel nous détaillerons quelques-unes des
choses plus complexes que l'on peut faire avec l'éditeur mais commençons
par une introduction.

L'éditeur peut vous sembler être une petite boîte morne mais ne laissez
pas son apparence vous décevoir : sous son visuel minimaliste, se cache
une mine de fonctionnalités que vous pouvez activer en cliquant à
l'intérieur de la boîte.

![Publisher](images/partage-1.png)

Dès que la boîte de l'éditeur est activée, vous pouvez y voir un
ensemble de boutons et d'icônes. Avant de les examiner dans le détail,
ignorons-les et publions un simple message de statut qui ne sera
disponible que par ceux qui vous suivent et vos contacts mutuels.

Pour cela, tout ce que vous avez à faire est de taper votre message –
quoique vous ayez envie de leur dire, par exemple "Ceci est mon premier
message sur Diaspora" – et d'appuyer sur le bouton Partager. Ça y est,
vous avez partagé un message avec vos contacts.

ALORS, que-ce que c'est que tous ces boutons ?

### Formatage du texte

Au-dessus de la zone de publication, vous verrez deux onglets et des boutons. Les onglets correspondent au texte brut que vous écrivez et à sa prévisualisation. Les boutons sont là pour vous aider à formater votre texte :

* Gras
* Italique
* Titres
* Liens
* Image
* Liste à puces
* Liste ordonnée
* Du code
* Des citations

### Bouton de sélection des aspects

C'est avec ce bouton que vous pouvez choisir qui pourra lire votre
message. Il est réglé sur "Tous les aspects" par défaut : toutes les
personnes que vous avez ajoutées à l'un de vos aspects sera susceptible
de lire le message et personne d'autre. Dans le message tout simple que
vous venez d'envoyer, il n'était pas nécessaire de changer le moindre
réglage puisque vous le destiniez à vos suiveurs et que le réglage par
défaut est "Tous les aspects".

Ce bouton vous permet de choisir le ou les aspect(s) avec lesquels vous
souhaitez partager en cliquant sur les aspects de la liste pour les
(dé)sélectionner. De cette façon, vous contrôlez totalement qui peut
avoir accès à vos messages, comme nous l'avons vu dans la [Partie
3](3-aspects.html). Si, en
revanche, vous souhaitez annoncer quelque chose au monde entier,
choisissez "Public" et il n'y aura aucune restriction quant aux
personnes susceptible de lire votre message.

### Ajouter un sondage

![Aspects select](images/partage-2.png)

L'icône dans le coin inférieur droit de la zone de publication qui ressemble à un graphique permet d'ajouter un sondage à un post. Cliquez dessus et une fenêtre s'ouvrira dans laquelle vous pourrez entrer la question que vous souhaitez poser. A chaque réponses que vous ajoutez, une nouvelle s'ajoute, ainsi, vous pouvez entrer autant de possibilité de réponses que vous souhaitez.

### Partage de photos

En bas à droite du champ de saisie, se trouve une icône d'appareil photo
qui vous permet d'ajouter des photos à votre message. Vous pouvez soit
cliquer dessus et choisir des photos présentes sur votre ordinateur soit
faire glisser des images depuis un dossier vers ce bouton.

### Localisateur

À côté de l'appareil photo se trouve une icône de "punaise" qui active
la fonction de localisation. Cela vous permet d'ajouter une localisation
à vos messages. Cliquez dessus et il vous sera demandé si vous souhaitez
que votre position soit déterminée en utilisant OpenStreetMap et, si
vous êtes d'accord, une note indiquant votre position sera ajoutée à
votre message.

Vous pouvez modifier cette localisation en cliquant sur le texte
lorsqu'il apparaît sous la fenêtre de l'éditeur.

La localisation fournie peut être très précise et vous pouvez souhaiter
la modifier pour retirer l'adresse exacte et n'indiquer que la ville ou
la région où vous vous trouvez. C'est vous qui décidez.

### Services connectés

Selon les connexions que vous avez établies avec vos comptes sur
d'autres réseaux sociaux (Facebook, Twitter et Tumblr), les icônes de
ces services peuvent apparaître sous l'éditeur. En mettant ces icônes en
surbrillance, votre message sera publié sur ces services. Nous verrons
cela dans la section suivante, un peu plus bas.

L'icône à côté des services connectés ressemble à une clé à molette.
Cliquez dessus pour configurer les connexions avec d'autres réseaux
sociaux et services.

Nous verrons comment établir une connexion avec d'autres services dans
la [Partie 7](7-fin.html).

C'est tout ce qu'il y a à savoir au sujet des boutons et des icônes pour la zone de publication. La réelle magie s'opère dans la zone de publication elle-même. En plus du texte brut, vous pouvez le formater, ajouter des \#tags, @mentions, des liens, insérer des photos, vidéos, audio. Wow !

## Fonctionnalités de publication

### \#tags

Placez un symbole \# (dièse) devant un mot et il devient un \#tag. À
quoi cela sert-il ? Eh bien, les \#tags sont un excellent moyen de
trouver du contenu qui vous intéresse et d'attirer l'attention des
personnes qui sont susceptibles de s'y intéresser. Vous pouvez effectuer
une recherche par tags en les tapant dans la zone de recherche, dans la
zone d'en-tête. Vous pouvez aussi suivre des tags de sorte que n'importe
quel message qui contiendra ce tag apparaîtra dans votre flux – de fait,
si vous avez renseigné la partie "Décrivez-vous en 5 mots" de votre page
de profil, vous suivez déjà ces tags !

Supposons que vous vous intéressiez à la cuisine et que vous publiiez un
message contant "\#cuisine". Tous ceux qui suivent le tag \#cuisine
verront votre message apparaître dans leur flux. Bien sûr, pour que les
gens qui suivent le tag \#cuisine voient votre message, vous aurez dû le
rendre public ; si vous l'avez limité à seulement certains aspects,
seules les personnes de ces aspects y auront accès. Vous pouvez
également rechercher \#cuisine dans la barre de recherche et vous seront
présentés une liste de messages tagués avec "\#cuisine". Cela inclut vos
propres messages, ceux de vos contacts et les messages publics des
autres utilisateurs de diaspora\*. Vous voyez à quel point cela peut
être un outil fantastique pour trouver et partager du contenu avec des
gens qui partagent vos centres d'intérêt.

Si votre message peut potentiellement offenser ou causer du tort à
quelqu'un qui le regarderait au travail, merci d'ajouter le tag \#nsfw
(“not safe for work” = "pas sûr pour le travail") afin qu'il soit
masqué, sauf si les personnes choisissent de le voir. Nous reviendrons
là-dessus dans la [Partie
7](7-fin.html).

### @Mentions

Saviez-vous que vous pouvez attirer l'attention de quelqu'un en le/la
mentionnant ? Ajoutez simplement le symbole @ suivi du nom d'un de vos
contacts. À mesure que vous tapez son nom (son pseudo, pas son nom
d'utilisateur) il sera automatiquement complété par diaspora\*. Appuyez
sur ou cliquez sur le nom dans la fenêtre d'auto-complétion et vous
verrez qu'il sera changé en son nom complet et que le symbole @ aura
disparu. Ne vous inquiétez pas, il redeviendra une @mention lorsque vous
publierez le message. La personne que vous mentionnez recevra une
notification de cette mention sur sa page de notifications et par email
si elle a choisi de recevoir les notifications de mentions par email.

![Mentions](images/partage-3.png)

De même, d'autres personnes peuvent vous @mentionner et vous recevrez
une notification dans votre barre d'en-tête (et par email si vous avez
réglé cette option).

Notez que vous ne pouvez @mentionner que des personnes présentes dans
vos aspects et uniquement dans des messages, pas dans des commentaires.

### Formatage du texte

Plusieurs possibilités : gras, italique, titres etc… La plupart des formatages peuvent être fait avec les boutons de la zone de publication. Il n'est pas nécessaire de connaître la syntaxe Markdown.

Pour ajouter de *l'italique*, du **gras** our du code *en ligne*, sélectionnez le texte que vous souhaitez formater et cliquez sur le bouton correspondant. Le code Markdown se placera autour du texte sélectionné.

Pour créer des titres, listes ou citations, placez votre curseur à la ligne que vous souhaitez formater et cliquez sur le bouton souhaité.

### Liens

Vous souhaitez insérer un lien dans un post  ? Cliquez simplement sur le bouton « lien » et collez-y l'URL dans la *pop-up*. Cela insérera le lien dans le post, entouré par le code Markdown. Tapez ensuite le texte (à la place du texte sélectionné) que vous souhaitez voir apparaître dans les crochets.

Si vous souhaitez insérer un lien brut, collez simplement l'URL dans la zone de publication et il sera automatiquement converti en lien. Vous n'avez pas à utiliser de syntaxe Markdown, sauf si vous souhaitez lui donner du style !

### Images

Incorporer une image depuis Internet dans votre post est similaire à l'insertion d'un lien. Cliquez sur le bouton « image » au-dessus de la zone de publication et collez l'URL de l'image dans la *pop-up* qui apparaît. Cela insérera l'URL dans le post, entouré par le code Markdown. Tapez un « texte alternatif » (le texte que vous souhaitez voir apparaître si l'image ne s'affiche pas/plus) à la place du texte sélectionné entre les crochets. Vous pouvez aussi ajouter un titre, qui sera affiché au survol de la souris (optionnel).

Note : pour que cela fonctionne, vous devez insérer un lien direct vers l'image (finissant par ``.jpg``, ``.gif``, ``.png`` ou autres), **non pas** le lien de la page qui contient la/les image(s).

diaspora\* utilise Markdown pour formater. Si vous souhaitez formater plus finement qu'avec les boutons, vous trouverez plus d’information dans [ce
tutoriel](https://diasporafoundation.org/formatting).

### Intégration

Vous pouvez également coller des liens vers des sites de médias comme
YouTube, Vimeo, Soundcloud et autres et la cible vidéo/audio de votre
lien apparaîtra comme intégrée à votre message. Essayez !

Notez que la vidéo/l'audio ne sera réellement intégrée qu'*après* que
vous ayez publié votre message, vous devrez donc actualiser/recharger le
flux pour la voir. Parfois le site qui héberge le fichier que vous
souhaitez intégrer peut mettre un peu de temps à répondre , vous devrez
donc attendre un peu et actualiser le flux à nouveau.

### Publier vers d'autres services

Si vous avez connecté votre graine diaspora\* à vos comptes sur d'autres
services, l'action de cliquer sur une ou plusieurs icônes de ces
services en bas de l'éditeur provoquera la publication de votre message
sur ces réseaux également.

![Connected
services](images/partage-4.png)

Quand vous commencez à taper un message avec une ou plusieurs de ces
icônes "allumées", un compteur vous indique le nombre de caractères
restant : 140 pour Twitter, 1000 pour Tumblr, 63 206 pour Facebook. Sur
diaspora\*, vous pouvez publier jusqu'à 65 535 caractères ! Le compteur
indiquera toujours le nombre de caractères restant pour le service
sélectionné le plus restrictif ; donc si vous avez sélectionné les logos
de Twitter et Facebook, le décompte partira de 140 caractères.

Si votre message est plus long que ce qui est permis sur le(s)
service(s) sur le(s)quel(s) vous publiez, il sera tronqué sur ce(s)
service(s) et contiendra un lien vers votre publication sur diaspora\*.

### Prévisualisation

Entre le bouton des aspects et le bouton de partage, se trouve un
dernier bouton – et il est très utile ! Grâce à lui, vous pouvez voir à
quoi ressemblera votre message et être sûr(e) qu'il vous convient avant
de le publier pour de bon. Cela peut vraiment vous aider à corriger les
erreurs, notamment en ce qui concerne le formatage du texte. Nous allons
y revenir.

Voilà tout ce qu'il y a à savoir sur les boutons, les icônes et
l'éditeur. Toute la magie est liée à l'éditeur lui-même. Vous pouvez, en
plus du texte brut, ajouter des \#tags, des @mentions, des liens,
formater votre texte et insérer des photos, des vidéos et de l'audio.
Wow !

![Publisher content](images/partage-5.png)

![Publisher preview](images/partage-6.png)

Que dites-vous de ça ? Il y a plein de choses que vous pouvez faire dans
vos messages sur diaspora\*.

Pour essayer tout ça, pourquoi ne pas annoncer publiquement que vous
venez d'arriver sur diaspora\* ? C'est une bonne façon d'être
accueilli(e) dans notre communauté et de commencer à établir des
connexions.

Pour écrire un message public, réglez simplement le bouton de sélection
des aspects sur Public et écrivez votre message. Si vous ne l'avez déjà
fait, pourquoi ne pas vous présente à la communauté diaspora\* en
incluant le tag \#nouveauici ? De cette façon, les personnes qui suivent
ce tag verront votre message et pourront vous accueillir. Incluez les
\#tags de quelques-uns de vos centres d'intérêt et vous verrez que les
personnes qui ont des intérêts similaires vous trouveront. Votre message
pourrait ressembler à :

> Bonjour à tous, je suis \#nouveauici sur diaspora\*. J'aime \#courir,
> la \#littérature et les \#lapins.

Bien sûr, vous pouvez très bien ne pas vouloir rendre ce message public.
Ça n'est pas un problème : ce qui est important c'est que vous sachiez
comment le faire.

## Interagir avec les messages dans le flux

Le flux est un affichage temps-réel de messages de statut. Dès qu'une
personne que vous suivez publie un message de statut, il s'ajoute à
votre flux. Naturellement, vous pouvez commenter vos propres messages
ainsi que ceux publiés par d'autres personnes. Vous pouvez également
"aimer" et "repartager" les messages d'autres personnes et, si quelqu'un
publie quelque-chose que vous ne voulez vraiment pas voir, vous pouvez
le masquer. Tout ceci se passe dans le flux qui se trouve juste
en-dessous de l'éditeur.

N'oubliez pas qu'il y a différents flux de contenu en fonction de la
"vue" que vous utilisez : Flux, Mon activité, @Mentions, Mes aspects ou
\#tags suivis. Bien que cette vue altère quels messages apparaissent
dans votre flux, vous pouvez interagir avec ces messages de la même
façon, quelle que soit la vue.

Pour améliorer la vitesse de chargement sur votre pod, votre flux ne sera pas automatiquement rafraîchi. Rafraîchissez votre page lorsque vous souhaitez voir les nouveaux posts.

### Commentaires

S'il n'y a pas encore de commentaire sur un message, cliquez sur
Commenter pour ouvrir un champ de commentaire. Lorsqu'il y a déjà des
commentaires sous le message, ce champ est automatiquement affiché. Si
plus de trois commentaires existent seul les trois derniers sont
présentés. Vous pouvez déployer l'ensemble du fil de discussion en
cliquant

### J'aime

À côté du lien Commenter, vous trouverez un lien J'aime. Il permet
d'indiquer à l'auteur(e) du message que vous avez lu et apprécié son
billet sans pour autant le commenter. Quand vous cliquez sur le lien,
vous serez ajouté(e) au compteur du nombre de "j'aime". En cliquant sur
ce compteur, les noms des personnes qui ont aimé le message
apparaîtront.

Si vous changez d'avis, vous pouvez retirer votre "J'aime" en cliquant
sur le lien Je n'aime plus qui sera apparu. Mais sachez que la personne
qui a publié le message que vous aviez aimé a déjà reçu la notification
de votre "J'aime".

### Repartager des messages

Les billets publics envoyés par d'autres personnes peuvent être
repartagés. Pour ce faire, cliquez sur le lien Repartager qui se trouve
en-dessous des liens "J'aime" et "Commenter". Repartager un message le
rend public aux gens qui vous suivent et peut aider à diffuser un bon
message à une plus large audience.

 

Lorsque vous survoler un post, des icônes apparaissent dans le coin supérieur droit de celui-ci :

### Effacer des messages et des commentaires

Quand votre souris survole l'un de vos messages ou l'un de vos
commentaires, une icône symbolisant une poubelle (![sphere supprimer commentaire](images/sphere_supprimer_commentaires.png)) apparaît dans le coin supérieur droit et vous permet
de supprimer votre message ou commentaire. Vous pouvez également effacer
vos propres commentaires sur les messages d'autres personnes.

### Cacher la publication de quelqu'un

Si vous ne souhaitez plus voir les publications de quelqu'un, vous
pouvez les masquer de votre flux en cliquant sur le `x`, de la même façon
que vous supprimeriez votre propre message.

### Activer/désactiver les notifications

A côté du ``x`` se trouve une petite cloche. Si cette cloche est grise, cela signifie que vous ne recevez pas les notifications pour ce post. Cliquez sur l'icône pour qu'elle devienne noire, et donc recevoir les notifications pour ce post. Si vous recevez les notifications et que vous souhaitez ne plus les recevoir, cliquez sur la cloche pour qu'elle redevienne grise.

### Ignorer un utilisateur

Si vous trouvez que les messages de quelqu'un sont dérangeants, ennuyeux
ou vous offensent, vous pouvez ignorer cette personne. Cela signifie que
ses messages n'apparaîtront plus dans votre flux ; toutefois, il sera
toujours en mesure de partager vos messages et ses
commentaires sur vos messages et ceux des autres personnes vous seront
toujours visibles. Pour ignorer quelqu'un, passez votre souris sur l'un
de ses messages et cliquez sur l'icône ![ignorer post](images/sphere_ignorer_post.png) dans le coin
supérieur droit. Quand vous ignorez quelqu'un, ses messages ne disparaissent pas
immédiatement ; vous devez actualiser la page.

Vous pouvez aussi ignorer quelqu'un en allant sur son profil et en cliquant sur l'icône correspondante : ![ignorer user profil](images/sphere_ignorer_user.png)

Vous pouvez trouver une liste des personnes que vous ignorez dans les
paramètres de votre compte, dans la section [Vie privée](https://framasphere.org/privacy). Si vous
souhaitez arrêter d'ignorer quelqu'un, vous pouvez l'enlever depuis
cette page.

### Signaler le post de quelqu'un

Si vous pensez qu'un post enfreint les conditions d'utilisation du *pod*, vous pouvez cliquer sur le bouton de signalement (![signaler un post](images/sphere_signalement.png)). Apparaîtra un formulaire qui sera envoyé à votre *podmin*. Vérifiez bien que le post enfreint les [conditions d'utilisation](https://framasphere.org/terms) avant de faire un signalement. Vous trouverez un lien dans la barre latérale gauche.

Vous avez maintenant appris ce qu'il y a à savoir sur votre activité
principale sur diaspora\* : publier des messages de statut et commenter
ceux d'autres personnes. Dans la partie suivante de ce tutoriel, nous
allons nous intéresser aux conversations : les messages privés échangés
avec un ou plusieurs contacts.

[4ème partie - Trouver des gens et se connecter avec
eux](4-echange.html) |
[Sixième partie - Notifications et
conversations](6-notification-conversation.html)

### Ressources utiles

-   [Base de code](http://github.com/diaspora/diaspora/)
-   [Documentation](https://wiki.diasporafoundation.org/)
-   [Trouver et signaler des
    bugs](http://github.com/diaspora/diaspora/issues)
-   [IRC - Général](http://webchat.freenode.net/?channels=diaspora)
-   [IRC -
    Développement](http://webchat.freenode.net/?channels=diaspora-dev)
-   [Liste - Général](http://groups.google.com/group/diaspora-discuss)
-   [Liste - Développement](http://groups.google.com/group/diaspora-dev)

[![Licence Creative
Commons](images/cc_by.png)](http://creativecommons.org/licenses/by/3.0/)
[diasporafoundation.org](https://diasporafoundation.org/) est protégé
sous licence [Licence publique Creative Commons Attribution 3.0 non
transposée](http://creativecommons.org/licenses/by/3.0/)
